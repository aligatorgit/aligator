<?php 
  $body = @$node->body['und'][0]['safe_value'];
  $images = array();
  foreach($node->field_images['und'] as $image){
    $images['big'][] = image_style_url('big', $image['uri']);
    $images['medium'][] = image_style_url('medium', $image['uri']);
    $images['small'][] = image_style_url('small', $image['uri']);
    $images['product'][] = image_style_url('product', $image['uri']);
  }
  $number = @$node->field_number['und'][0]['value'];
  $price = @$node->field_price['und'][0]['value'];
  $delivery = @$node->field_delivery['und'][0]['value'];
  $condition = @$node->field_condition['und'][0]['value'];
?>

<?php if($page): ?>
<div class="product" id="product-<?php print $node->nid ?>" itemscope itemtype="http://schema.org/Product">
  <h1 itemprop="name"><?php print $title ?></h1>
  <div class="product-images">
    <div class="image"><a class="fancybox" rel="group" href="<?php print $images['big'][0] ?>"><img itemprop="image" src="<?php print $images['medium'][0]; ?>" width="400" height="300" alt="<?php print $node->title ?>" /></a></div>
    <div class="slider-photo">
        <div class="slider-prev"></div>
        <div class="slider">
          <ul>
            <?php foreach($images['small'] as $k=>$img): ?>
            <?php if(!$k) continue; ?>
            <li><a class="fancybox" rel="group" href="<?php print $images['big'][$k] ?>"><img src="<?php print $img; ?>" width="128" height="96" alt="<?php print $node->title ?>" /></a></li>
            <?php endforeach; ?>
          </ul>
        </div>
        <div class="slider-next"></div>
    </div>
  </div>
  <div class="product-info">
    <div class="product-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <?php if($price): ?>
      <div>Цена в Польше:</div>
      <div class="price"><span itemprop="price"><?php 
        print currency_exchange($price)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></div>
      <?php endif; ?>
      <?php if($delivery): ?>
      <div class="bid">Доставка в Польше: <b><?php 
        print basic_cart_price_format(currency_exchange($delivery));
      ?></b></div>
      <?php endif; ?>
    </div>
    <div class="product-rate">
      Для уточнений стоимости доставки в Украину и комиссии обратитесь к менеджеру
    </div>
    <?php 
      print allegro_basic_cart_add_to_cart_button(array(
        'nid' => $node->nid
      )); 
    ?>
    <?php if($condition): ?>
    <div class="product-condition">Состояние товара: <?php print $condition ?></div>
    <?php endif; ?>
    <?php if($number): ?>
    <div class="product-id">Номер товара: <?php print $number ?></div>
    <?php endif; ?>
  </div>
  <?php if($body): ?>
  <div class="product-description">
    <h3>Описание товара</h3>
    <div class="padding field-name-body" itemprop="description"><?php print $body ?></div>
  </div>
</div>
<?php endif; ?>
<?php else: ?>
<div class="search-result row-<?php print $node->nid ?>" itemscope itemtype="http://schema.org/Product">
  <a target="_blank" href="<?php print $node_url ?>" class="clearfix">
    <span class="image"><img itemprop="image" src="<?php print $images['product'][0] ?>" alt="<?php print $node->title ?>" title="<?php print $node->title ?>" width="128" height="96" /></span>
    <span class="title" itemprop="name"><?php print $node->title ?></span>
    <?php if($condition): ?>
    <span class="condition">Состояние: <b><?php print $condition ?></b></span>
    <?php endif; ?>
    <?php if($number): ?>
    <span class="number">Номер товара: <b><?php print $number ?></b></span>
    <?php endif; ?>
    <span class="price-block" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <?php if($price): ?>
      <span class="price"><span itemprop="price"><?php 
        print currency_exchange($price)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></span>
      <?php endif; ?>
      <?php if($delivery): ?>
      <span class="bid">Доставка по Польше: <b><?php 
        print basic_cart_price_format(currency_exchange($delivery))
      ?></b></span>
      <?php endif; ?>
    </span>
  </a>
</div>
<?php endif; ?>