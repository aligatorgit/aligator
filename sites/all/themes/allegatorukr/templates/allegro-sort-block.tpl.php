<?php /* <div class="search-filter">
Отображать по: <?php print allegro_link(10, 'limit', 10); ?> <?php print allegro_link(20, 'limit', 20); ?> <?php print allegro_link(30, 'limit', 30); ?>
</div> */ ?>
<div class="search-order">
  <form<?php if(drupal_is_front_page()) print ' action="search"'; ?>>
  	<div class="form-item form-type-select form-item-subtype">
    	<?php
      	$options = array(
          'endingTime' => 'По дате окончания',
          'price' => 'По цене',
          'popularity' => 'По популярности',
        );
        $option = allegro_arg('sort');
      ?>
      <span class="select-wrapper"><span><?php print $options[$option] ?></span><select onchange="javascript:this.parentNode.firstChild.innerHTML=this.options[this.selectedIndex].innerHTML;this.form.submit()" id="edit-sort" name="sort" class="form-select"><?php
        foreach($options as $value => $title) {
          $selected = $option == $value ? ' selected="selected"' : '';
          print '<option value="'.$value.'"'.$selected.'>'.$title.'</option>';
        }
      ?></select></span>
    </div>
    <?php 
      foreach($_GET as $name=>$value){
        if(in_array($name, array('q', 'sort', 'limit'))) continue;
        print '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
      }  
    ?>
  	<input type="submit" value="Фильтровать">
  </form>
</div>