<nav>
	<div class="wrap">
    <?php print theme('links', array('links' => $main_menu)); ?>
	</div>
</nav>
<header>
	<div class="wrap clearfix">
  	<a<?php if(!$is_front): ?> href="/"<?php endif; ?> class="logo">Allegator</a>
  	<div class="form">
    	<?php drupal_add_library('system', 'drupal.autocomplete'); ?>
  	  <form class="search-form advanced-autocomplete" action="/search">
    	  <div class="form-item form-type-textfied form-item-search">
    	    <input type="text" id="edit-search" name="search" placeholder="Что ищите?" value="<?php print allegro_search_string(); ?>" class="form-autocomplete" autocomplete="off" aria-autocomplete="list" />
    	  </div>
  	    <div class="form-item form-type-checkbox form-item-description">
          <input type="checkbox"<?php if(allegro_arg('description')) print ' checked="checked"' ?> id="edit-in-description" name="description" value="1" class="form-checkbox"><label class="option" for="edit-in-description">Искать в описании товара</label>
        </div>
    	  <input type="hidden" id="edit-search-autocomplete" value="/search/autocomplete" disabled="disabled" class="autocomplete">
    	  <?php foreach($_GET as $name => $value): ?>
    	  <?php if($name == 'q') continue; ?>
    	  <input type="hidden" name="<?php print $name ?>" value="<?php print $value ?>">
    	  <?php endforeach; ?>
    	  <input type="submit" value="" />
  	  </form>
      <div class="phones">
        <a href="tel:8800756245" class="free">38 (097) 270-46-74</a>
        <a href="tel:380501234567;" class="viber">38 (050) 270-46-74</a>
        <a href="tel:380631234567" class="whats">38 (056) 736-00-30</a>
      </div>
  	</div>
	  <?php
  	  ctools_include('modal');
  	  ctools_modal_add_js();
  	  $price = basic_cart_get_total_price();
      $options = array(
        'cart' => basic_cart_get_cart(),
        'price' => $price->total,
      );
      print theme('basic_cart_cart_render_block', $options);
	  ?>
  	<div class="spares">
  	  <?php
  	    $form = drupal_get_form('allegro_spares_form');
  	    print render($form);
  	  ?>
  	</div>
	</div>
</header>
<section class="main-content">
	<div class="wrap">
  	<header>
        <?php if(!drupal_is_front_page()): ?>
            <h1><?php print $title; ?><?php if($item_id): ?> <small>(<?php print $item_id; ?>)</small><?php endif; ?></h1>
        <?php endif; ?>
    	<?php if($original_title): ?><div class="subtitle"><?php print $original_title; ?></div><?php endif; ?>
    	<?php if(!$is_front): ?><?php print $breadcrumb; ?><?php endif; ?>
  	</header>
    <?php if($messages): ?>
    <div class="messages-wrapper">
      <?php print $messages; ?>
    </div>
    <?php endif; ?>
  	<div class="columns clearfix">
    	<?php if($page['left']): ?>
    	<div class="left-column">
      	<?php print render($page['left']); ?>
    	</div>
      <?php endif; ?>

    	<?php if($page['right']): ?>
    	<div class="right-column">
      	<?php print render($page['right']); ?>
    	</div>
    	<?php endif; ?>

    	<div class="main-column">
      	<?php print render($page['content']); ?>
        <?php if(drupal_is_front_page()): ?>
            <br>
            <h1>Авторазборки Украина</h1>
            <p>Автомобиль изменил нашу жизнь, и хоть приятных моментов он приносит значительно больше, иногда случаются
                неприятности, порой весьма затратные по времени и финансам. При этом львиная доля затрат приходится на
                наценки, которые, порой, увеличивают стоимость деталей в 2-3 раза, по сравнению с ее изначальной
                ценой.</p><p>Именно поэтому, <strong>интернет магазин автозапчастей</strong> из Польши Allegator станет
                отличным выбором!</p>
            <br>
            <h2>Ассортимент автомобильных запчастей</h2>
            <p>Мы предлагаем обширный <strong>онлайн-каталог
                    деталей</strong> на машин европейских и мировых производителей, которые выставляются на продажу на
                крупнейшем аукционе в польско-язычном интернете – <strong>Allegro.pl</strong>. Это удобная <strong>авторазборка</strong>
                с возможностью доставки выбранных деталей по нужному вам адресу.</p><p>Бережливость польских
                автовладельцев делает очень привлекательными объявления о продаже деталей и агрегатов с машин, бывших в
                употреблении. По статистике, средний срок эксплуатации автомобилей в странах Евросоюза составляет 5-7
                лет. Более того, во время поездок они подвергаются меньшим нагрузкам по причине высокого качества дорог,
                даже в удаленных уголках страны. Прибавьте к этому отличный сервис с четким соблюдением нормативов
                замены масла и других технических жидкостей, и вы поймете, что заказанные в нашем магазине польские
                детали прослужат вам еще долгое время.</p>
            <br>
            <h2>Оформление заказа в интернет магазине авторазборки</h2>
            <p>
                Функционал нашего сайта позволит вам отыскать нужную <strong>деталь б/у</strong>, после чего будет
                организована&nbsp;<strong>доставка автозапчастей из Польши в Украину</strong> в кратчайшие сроки.</p><p>
                Удобная форма поиска и каталог позволят подобрать интересующие детали, полностью соответствующие именно
                вашему автомобилю. Доступны элементы как для новых моделей, так и для старых, снятых с производства. При
                необходимости наши специалисты помогут выбрать детали, и дадут максимально полную информацию о
                существующих аналогах, преимуществах работы с польскими продавцами.</p><p>Каталог <strong>интернет-магазина
                    Allegator</strong> поможете выбрать и <strong><a href="https://allegator.com.ua/cat/620"
                                                                     target="_blank">купить запчасти с
                        Польши</a></strong> для всех систем и узлов машины:</p><p>- двигатели внутреннего сгорания;</p>
            <p>- <a href="https://allegator.com.ua/cat/49236" target="_blank">автомобильные фильтры</a>;</p><p>-
                трансмиссии (в том числе автоматической коробки переключения передач);</p><p>- <a
                        href="https://allegator.com.ua/cat/8684" target="_blank">амортизаторы</a>;</p><p>- выхлопы;</p>
            <p>- <a href="https://allegator.com.ua/cat/18834" target="_blank">тормозные системы</a>;</p><p>- свет и
                автоэлектроника;</p><p>- охлаждающие системы;</p><p>- колеса, диски, колпаки и т.д.</p><p>После заказа
                на сайте наши специалисты доставят его в Украину, по прибытии проверят соответствие заявленному
                качеству, комплектности, износу, и отправят по указанному Вами адресу.</p>
            <br>
            <h2>Цены на автозапчасти и детали б/у</h2>
            <p>Ответим также на два наиболее распространенных вопроса о стоимости и сроках доставки
                запчастей.</p><p><strong>Цены на запчасти б/у из Польши</strong>, указанные на сайте, в точности
                соответствуют ценам на польской онлайн-авторазборке. При этом стоимость доставки зависит от следующих
                факторов:</p><p>- суммарного расстояния между пунктом продажи и получателем;</p><p>- габаритов и
                характера груза;</p><p>- требования к ускоренной доставке;</p><p>- общей стоимости заказа. </p><p>Мы
                предоставляем скидки и специальные условия крупным заказчикам.</p><p>В стоимость включаются затраты
                на:</p><p>- упаковку запчастей (в том числе обрешетку, засыпку пенопласта и других наполнителей);</p><p>
                - страховку груза (в среднем 0,5-1% от заявленной стоимости).</p><p>Сроки доставки также зависят от
                месторасположения продавца и покупателя, размеров и характера товара. Зачастую общее время между
                осуществлением заказа и получением детали составляет 7-10 дней.&nbsp;</p><p>Крупные и тяжелые элементы
                (двигатель, кузов и его части) могут доставляться несколько дольше по причине поиска подходящей
                транспортной компании.</p><p><br></p><p>Обратите внимание! мы работаем по частичной предоплате: 20%
                стоимости оплачивается в момент заказа, остаток – в момент получения в транспортной компании по месту
                прибытия.</p>
            <br>
            <p>Автор:&nbsp;<a href="https://plus.google.com/108871635011277893833?rel=author" target="_blank">Allegator</a></p>
        <?php endif; ?>
    	</div>
  	</div>
	</div>
</section>
<footer>
	<div class="wrap clearfix">
  	<a class="logo">Allegator</a>
  	<?php print theme('links', array('links' => $secondary_menu)); ?>
    <div class="phones">
      <a href="tel:8800756245" class="free">38 (097) 270-46-74</a>
      <a href="tel:380501234567;" class="viber">38 (050) 270-46-74</a>
      <a href="tel:380631234567" class="whats">38 (056) 736-00-30</a>
    </div>
    <div class="copy">
      © 2010-<?php print date('Y') ?>. Все права защищены. Allegator
    </div>
	</div>

    <script type="application/ld+json">
        {
            "@context": "http://schema.org",
            "@type": "LocalBusiness",
            "url": "https://allegator.com.ua",
            "email": "allegator.parts@gmail.com",
            "name": "Allegator",
            "logo": "https://allegator.com.ua/sites/all/themes/allegatorukr/logo.png",
            "image": "https://allegator.com.ua/sites/all/themes/allegatorukr/logo.png",
            "address": [{
              "@type": "PostalAddress",
              "streetAddress": "Пуховская, 2А",
              "addressLocality": "Киев",
              "addressRegion": "Kyiv city",
              "postalCode": "02225",
              "addressCountry": "UA"
            },{
              "@type": "PostalAddress",
              "streetAddress": "Чугуевская, 80",
              "addressLocality": "Харьков",
              "addressRegion": "Kharkov oblast",
              "postalCode": "61140",
              "addressCountry": "UA"
            }],
            "geo": {
              "@type": "GeoCoordinates",
              "latitude": 50.5076897,
              "longitude": 30.6301439
            },
            "telephone": ["+38(097)270-46-74","+38(050)270-46-74","+38(056)736-00-30"],
            "openingHoursSpecification": {
                "@type": "OpeningHoursSpecification",
                "dayOfWeek": [
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday"
                  ],
                "opens": "09:00",
                "closes": "18:00"
            },
            "priceRange": "$1 - $50000",
            "sameAs" : ["https://plus.google.com/108871635011277893833"],
            "hasMap": ["https://goo.gl/maps/kECxVnpE3r82", "https://goo.gl/maps/GiVzvLhDMiy"]
        }
    </script>
</footer>
