<?php if (!$results): ?>
<p class="search-no-result">
  К сожалению по данному запросу ничего не найдено.
</p>
<?php else: ?>
<?php foreach ($results as $k=>$item): ?>
<div id="product-<?php print $item->id ?>" class="search-result clearfix" itemscope itemtype="http://schema.org/Product">  
	<a href="/itm/<?php print $item->id ?>" class="product<?php if (FALSE && $item->additionalInfo & 1) print ' verified'; ?> clearfix">
  	<?php if (!empty($item->images)) foreach (allegro_item_array($item->images) as $image): ?>
      <span class="image">
        <img 
          itemprop="image" 
          src="<?php print str_replace('/original/', '/s160/', $image->url) ?>" 
          alt="<?php print correct_tpr($item->name) ?>" 
          title="<?php print correct_tpr($item->name) ?>" 
          width="138" 
          height="104" />
      </span>
      <?php break; ?>
    <?php endforeach; ?>
  	<span class="title" itemprop="name"><?php print correct_tpr($item->name) ?></span>
    <span class="subtitle">(На польском: <?php print $item->name ?>)</span>
    <?php /* <span class="condition"><b>Состояние товара:</b> <?php print $item->conditionInfo == 'new' ? 'Новый' : 'Б/у'; ?></span> */ ?>
  </a>
  <div class="info">
    <span class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <span itemprop="availability" content="http://schema.org/InStock"></span>
      <span itemprop="url" content="<?php print url('/product/' . $item->id, array('absolute' => true)); ?>"></span>
      <?php if (FALSE && isset($item->endingTime)): ?>
      <span itemprop="priceValidUntil" content="<?php print $item->endingTime ?>"></span>
      <?php endif ?>            
      <?php if ($item->sellingMode->format == 'BUY_NOW'): ?>
      <span itemprop="price"><?php 
        print currency_exchange($item->sellingMode->price->amount)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span>
      <?php elseif ($item->sellingMode->format == 'AUCTION'): ?>
      <span itemprop="price"><?php 
        print currency_exchange($item->sellingMode->price->amount)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span>
      <?php endif; ?>
    </span>
    <?php 
      print allegro_basic_cart_add_to_cart_button(array(
        'nid' => $item->id
      )); 
    ?>
    <?php /*
    <span class="delivery">
      Доставка в Польше: 
      <?php if(isset($prices['withDelivery']) && $prices['withDelivery'] - $prices[$price_type] > 0): ?>
      <span class="price"><?php print $prices['withDelivery'] - $prices[$price_type] ?> PLN</span>
      <?php else: ?>
      <span class="free">Бесплатно</span>
      <?php endif; ?>
    </span>
    */ ?>
  </div>
</div>
<?php endforeach; ?>
<?php endif; ?>