<div id="wrapper">
  <header>
	  <a<?php if(!$is_front): ?> href="/"<?php endif; ?> id="logo">Allegro Украина</a>
	  <div id="ad">
	    <ul class="bjqs">
	      <li><img src="/<?php print path_to_theme() ?>/ads/ad1.png" width="315" height="85" alt="На прошлой неделе 119 клиентов получили свои посылки" /></li>
	      <li><img src="/<?php print path_to_theme() ?>/ads/ad2.png" width="315" height="85" alt="12794 клиентов выбрали нас" /></li>
	      <li><img src="/<?php print path_to_theme() ?>/ads/ad4.png" width="315" height="85" alt="Сервис доставки" /></li>
	      <li><img src="/<?php print path_to_theme() ?>/ads/ad5.png" width="315" height="85" alt="Выгодно покупать запчасти" /></li>
	    </ul>
	  </div>
	  <div class="telephone">
  	  <a href="tel:0674985454"><span>(067)</span> 498 54 54</a>
  	  <a href="tel:0991285454"><span>(099)</span> 128 54 54</a>
  	  <a href="tel:0935825454"><span>(093)</span> 582 54 54</a>
  	  <a href="tel:0567360030"><span>(056)</span> 736 00 30</a>
	  </div>
	  <div class="contacts">
	    <span class="skype"><a href="skype:all-pl?chat">all-pl</a></span>
  	  <a href="<?php print url('node/13'); ?>" class="callback use-ajax">Перезвоните мне</a>
	  </div>
  </header>
  <nav>
    <a href="#" class="show-menu"></a>
	  <?php
  	  ctools_include('modal');
  	  ctools_modal_add_js();
  	  $price = basic_cart_get_total_price();
      $options = array(
        'cart' => basic_cart_get_cart(),
        'price' => $price->total,
      );
      print theme('basic_cart_cart_render_block', $options);
	  ?>
    <?php print theme('links', array('links' => $main_menu)); ?>
  </nav>
  <?php if($messages): ?>
  <div id="messages">
    <?php print $messages; ?>
  </div>
  <?php endif; ?>
  <div id="content" class="clearfix">
	  <?php if($show_left): ?>
	  <div class="filters left">
      <div id="search" class="clearfix">
        <?php drupal_add_library('system', 'drupal.autocomplete'); ?>
    	  <form class="advanced-autocomplete" action="/search">
      	  <input type="text" id="edit-search" name="search" placeholder="Поиск по сайту" value="<?php print allegro_search_string(); ?>" class="form-autocomplete" autocomplete="off" aria-autocomplete="list" />
      	  <input type="hidden" id="edit-search-autocomplete" value="/search/autocomplete" disabled="disabled" class="autocomplete">
      	  <input name="in-description" type="hidden" value="<?php print allegro_arg('description') ?>" />
      	  <?php foreach($_GET as $name => $value): ?>
      	  <?php if($name == 'q') continue; ?>
      	  <input type="hidden" name="<?php print $name ?>" value="<?php print $value ?>">
      	  <?php endforeach; ?>
      	  <input type="submit" value="" />
    	  </form>
      </div>
  	  <div class="spares">
    	  <h3>Поиск <strong>запчастей</strong></h3>
    	  <?php
    	    $form = drupal_get_form('allegro_spares_form');
    	    print render($form);
    	  ?>
  	  </div>
	    <?php if($page['left']): ?>
	    <div class="filters-wrapper">
	    <?php print render($page['left']); ?>
	    </div>
	    <?php else: ?>
  	  <div class="categories">
    	  <h3>Популярные <strong>категории</strong></h3>
    	  <ul>
      	  <li><a href="/category/50849">Двигатель</a></li>
      	  <li><a href="/category/50871">Коробка передач</a></li>
      	  <li><a href="/category/4145">ЭБУ Блоки Управления</a></li>
      	  <li><a href="/category/8683">Детали ходовой</a></li>
      	  <li><a href="/category/4094">Детали кузова</a></li>
      	  <li><a href="/category/18730">Диски и шины</a></li>
      	  <li><a href="/category/622">Интерьер</a></li>
    	  </ul>
    	  <a href="/category" class="more">Все категории</a>
  	  </div>
      <?php endif; ?>
	  </div>
    <?php endif; ?>
	  <?php if($page['right']): ?>
	  <div class="filters right">
	    <div class="filters-wrapper">
  	  <?php print render($page['right']); ?>
	    </div>
	  </div>
	  <?php endif; ?>
    <?php if($is_front): ?>
    <div class="region-content store">
      <h2>Популярные товары</h2>
      <?php print views_embed_view('store', 'block'); ?>
      <a href="/store" class="more">Все популярные товары</a>
    </div>
    <div class="clear-content">
  	  <h1><?php print $title ?></h1>
  	  <?php print render($page['content']); ?>
    </div>
    <?php else: ?>
	  <div class="region-content">
      <?php if($breadcrumb): ?>
      <div id="breadcrumb"><?php print $breadcrumb; ?></div>
      <?php endif; ?>
      <?php if(arg(0) != 'product' && @$node->type != 'product'): ?>
  	  <h1><?php print $title ?></h1>
  	  <?php endif; ?>
  	  <?php print render($page['content']); ?>
	  </div>
	  <?php endif; ?>
  </div>
</div>
<footer>
  <div class="wrap">
    <div class="left">
      <a<?php if(!$is_front): ?> href="/"<?php endif; ?> class="logo">Allegro Украина</a>
	    <div class="social">
	      <?php //Поделиться в соцсетях: ?>
	      <div class="yashare-auto-init" data-yashareL10n="ru" data-yashareType="none" data-yashareQuickServices="vkontakte,facebook,twitter,odnoklassniki,moimir,gplus"></div>
	    </div>
	    <div class="text">© 2010-<?php print date('Y') ?>. Все права защищены. Запчасти из Польши.</div>
	    <?php /* <div class="text">Сделано в <a href="http://www.triartika.ru/">«ТриАртика»</a></div> */ ?>
    </div>
    <div class="right">
  	  <div class="telephone clearfix">
    	  <a href="tel:0674985454"><span>(067)</span> 498 54 54</a>
    	  <a href="tel:0991285454"><span>(099)</span> 128 54 54</a>
    	  <a href="tel:0935825454"><span>(093)</span> 582 54 54</a>
    	  <a href="tel:0567360030"><span>(056)</span> 736 00 30</a>
  	  </div>
  	  <?php print theme('links', array('links' => $secondary_menu, 'attributes' => array('class' => array('menu')))); ?>
    </div>
  </div>
</footer>