<?php

/**
 * Implementation of hook_breadcrumb().
 */
function allegatorukr_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    foreach($breadcrumb as $k => $row){
      if(strpos($row, '<a') !== FALSE){
        $text = strip_tags($row);
        $row = str_replace($text, '<span itemprop="name">' . $text . '</span>', $row);
        $row = str_replace('<a', '<a itemprop="item"', $row);

        $breadcrumb[$k] = '';
        $breadcrumb[$k] .= '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">';
        $breadcrumb[$k] .= $row;
        $breadcrumb[$k] .= '<meta itemprop="position" content="' . ($k+1) . '" />';
        $breadcrumb[$k] .= '</span>';
      } else {
        $breadcrumb[$k] = '<span>' . $row . '</span>';
      }
    }
    return '<div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">' . implode(' - ', $breadcrumb) . '</div>';
  }
}

/**
 * Implementation of hook_form_alter().
 */
function allegatorukr_form_alter(&$form, $form_state, $form_id) {
  switch($form_id) {
    case 'comment_node_page_form':
      global $user;

      $comment = $form_state['comment'];
      $node = $form['#node'];
      $anonymous_contact = variable_get('comment_anonymous_' . $node->type, COMMENT_ANONYMOUS_MAYNOT_CONTACT);
      $is_admin = (!empty($comment->cid) && user_access('administer comments'));

      $form['author']['mail']['#access'] = TRUE;
      $form['author']['mail']['#description'] = NULL;
      $form['author']['homepage']['#access'] = FALSE;
      $form['author']['_author']['#access'] = FALSE;

      if ($is_admin) {
        $author = (!$comment->uid && $comment->name ? $comment->name : $comment->registered_name);
      } else {
        if ($user->uid) {
          $author = $user->name;
        } else {
          $author = ($comment->name ? $comment->name : '');
        }
      }

      if(!user_access('administer comments')){
        $form['field_img']['#access'] = FALSE;
      }

      $form['author']['name'] = array(
        '#type' => 'textfield',
        '#title' => t('Your name'),
        '#title_display' => 'invisible',
        '#default_value' => $author,
        '#required' => (!$user->uid && $anonymous_contact == COMMENT_ANONYMOUS_MUST_CONTACT),
        '#maxlength' => 60,
        '#size' => 30,
        '#attributes' => array(
          'placeholder' => t('Your name'),
        ),
      );

      $form['author']['mail']['#title_display'] = 'invisible';
      $form['author']['mail']['#attributes']['placeholder'] = $form['author']['mail']['#title'];

      $form['author']['date']['#title_display'] = 'invisible';
      $form['author']['date']['#attributes']['placeholder'] = $form['author']['date']['#title'];

      if(!empty($form['captcha'])) $form['captcha']['#pre_render'][] = '_allegatorukr_captcha_pre_render';

      $form['author']['date']['#title_display'] = 'invisible';
      $form['author']['date']['#attributes']['placeholder'] = $form['author']['date']['#title'];

      $form['comment_body']['und'][0]['#title_display'] = 'invisible';
      $form['comment_body']['und'][0]['#attributes']['placeholder'] = $form['comment_body']['und'][0]['#title'];

      $form['actions']['submit']['#value'] = 'Отправить отзыв';
      break;
    case 'basic_cart_cart_form':
      $form['buttons']['update']['#attributes']['class'][] = 'form-update';
      break;
  }
}

/**
 * See allegatorukr_form_alter().
 */
function _allegatorukr_captcha_pre_render($element) {
  $element['captcha_widgets']['captcha_response']['#title_display'] = 'invisible';
  $element['captcha_widgets']['captcha_response']['#attributes']['placeholder'] = $element['captcha_widgets']['captcha_response']['#title'];
  $element['captcha_widgets']['captcha_response']['#description'] = NULL;
  return $element;
}

/**
 * Implementation of template_preprocess_page().
 */
function allegatorukr_preprocess_page(&$variables) {
  global $is_avtorazborka, $is_avtorazborka_term, $is_avtorazborka_page, $is_avtorazborka_node;

  if($is_avtorazborka_term) {
    $variables['page']['content']['system_main']['nodes']['#prefix'] = '<div class="node-article-wrapper clearfix">';
    $variables['page']['content']['system_main']['nodes']['#suffix'] = '</div>';
  }

  if($is_avtorazborka) {
    $variables['page']['left']['#markup'] = allegro_avtorazborka_category_allegro();
    $variables['page']['right']['#markup'] = allegro_avtorazborka_category_auto();

    if($is_avtorazborka_node) {
      $search = urlencode( allegro_search_string() );
      $items = cache_get('allegro:product:allegro:' . $search, 'cache');
      $variables['page']['content']['#suffix'] = theme('allegro_search_owl', array('results' => $items->data)) . $variables['page']['content']['#suffix'];
    }
  }

  $variables['item_id'] = !empty($variables['page']['content']['system_main']['#item']->itId) ? $variables['page']['content']['system_main']['#item']->itId : NULL;
  $variables['original_title'] = !empty($variables['page']['content']['system_main']['#item']->itName) ? $variables['page']['content']['system_main']['#item']->itName : NULL;

  if(drupal_is_front_page()) {
    if($cached = cache_get('allegatorukr:front', 'cache')) {
      $content = $cached->data;
    } else {
      $q = $_GET['q'];
      $_GET['q'] = 'search';
      $content = array(
        'main' => allegro_category(null),
        'left' => allegro_left_block(),
        'recommended' => allegro_recommended('', null),
      );
      $_GET['q'] = $q;
      
      if (strpos($content['left'], '<a href="/cat/') !== FALSE) {
        cache_set('allegatorukr:front', $content, 'cache', time() + 2*60*60);
      }
    }

    $variables['page']['content']['system_main']['products'] = array(
      '#weight' => 700,
      '#markup' => $content['main'],
    );
    
    if ($content['recommended']) {
      $variables['page']['content']['system_main']['recommended'] = array(
        '#weight' => 800,
        '#markup' => $content['recommended'],
      );
    }
    
    $variables['page']['left'] = $content['left'];


    $variables['page']['content']['system_main']['view'] = array(
      '#weight' => 900,
      '#markup' => '<div class="view view-last-articles"><h3>Статьи</h3>' . views_embed_view('blog', 'owl') . '</div>',
    );

    if(empty($variables['page']['content']['system_main']['nodes'][1]['#node']->body)) $variables['page']['content']['system_main']['nodes']['#access'] = FALSE;
  }
}

/**
 * Implementation of template_preprocess_views_view().
 */
function allegatorukr_preprocess_views_view(&$variables) {
  $variables['classes_array'][] = 'node-article-wrapper clearfix';
}

/**
 * Implementation of template_preprocess_html().
 */
function allegatorukr_preprocess_html(&$variables) {
  // Set binotel.ua script
  drupal_add_js('https://translate.google.com/translate_a/element.js?cb=googleTranslateElementInit', 'external');

  if (!user_access('administer modules') && !path_is_admin(current_path())) {
    $variables['binotel'] = "<script type=\"text/javascript\">(function(d, w, s) {var widgetHash = 'grQWvrk5W1', gcw = d.createElement(s); gcw.type = 'text/javascript'; gcw.async = true; gcw.src = '//widgets.binotel.com/getcall/widgets/'+ widgetHash +'.js'; var sn = d.getElementsByTagName(s)[0]; sn.parentNode.insertBefore(gcw, sn);})(document, window, 'script');</script>";
  } else {
    $variables['binotel'] = '';
  }

  // Set metateg on page, which has region element in url (like http://hostname.com/avtorazborka/audi/chernigov);
  global $title_region;
  $current_path = current_path();
  if ($title_region && arg(0) != 'node') {
    $term = menu_get_object('taxonomy_term', 2);
    $term_name = isset($term) ? $term->name : '';
    $term_tid = isset($term) ? $term->tid : '';
    $vid = 1;
    $all_terms = taxonomy_get_tree($vid, $parent = 0, $max_depth = 1);
    $tid_diff = array();
    foreach ($all_terms as $tid) {
      $tid_diff[] = $tid->tid;
    }
    //if (in_array($term_tid, $tid_diff)) {
      $rest = substr($title_region, -2);
      switch ($rest) {
        case 'ь':
        case 'а':
          $title_region = substr($title_region, 0, -2) . 'е';
          break;
        case 'о':
        case 'е':
          $title_region = $title_region;
          break;
        case 'ы':
          $title_region = substr($title_region, 0, -2) . 'ах';
          break;
        case 'й':
          $title_region = substr($title_region, 0, -4) . 'ом';
          break;
        default:
          $title_region = $title_region . 'е';
          break;
      }
      $variables['head_title'] = t("Dismantling @auto-model in @city | All dismantling @auto-model in Poland and Ukraine", array('@auto-model' => $term_name, '@city' => $title_region));
    //}
    } elseif ($title_region) {
      $variables['head_title_array']['title'] .= ' ' . $title_region;
      $variables['head_title'] = implode(' | ', $variables['head_title_array']);
    }
}
