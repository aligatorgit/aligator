jQuery.fn.simpleSilder = function(_option){
  var _options = jQuery.extend({
		step:2,
		duration:400,
		element:'li',
		root:'ul'
	}, _option);
	
  return this.each(function(){
    var _this = jQuery(this);
    var _leftArrow = _this.siblings('.slider-prev');
    var _rightArrow = _this.siblings('.slider-next');
    var _elements = jQuery(this).find(_options.element);
    var _element = _elements.first();
    var _root = _element.parents(_options.root);
    var _width = parseInt(_element.outerWidth()) + parseInt(_element.css('marginRight')) + parseInt(_element.css('marginLeft'));
    var _height = parseInt(_element.outerHeight());
    var _length = _elements.length;
    var _visibleWidth = parseInt(_root.parent().outerWidth());
    var _maxPosition = _length*_width-_visibleWidth;

    _this.css({
      height: _height + 'px',
      position: 'relative',
    });
    _root.css({
      position:  'absolute', 
      width:     _width*_length + 'px', 
      height:    _height + 'px',
      left:      '0px',
    });
    if(_length > _visibleWidth/_width) _rightArrow.addClass("active");
    _rightArrow.unbind('click').click(function(){
			var leftPosition = Math.abs(parseInt(_root.css("left")));
			leftPosition = leftPosition?leftPosition:0;
			if(leftPosition < _maxPosition && !_root.queue().length) {
				var step = _width*_options.step;
				if(leftPosition >= 0) _leftArrow.addClass("active");
				if(leftPosition+step >= _maxPosition) _rightArrow.removeClass("active");
				if(leftPosition+step > _maxPosition) step = _maxPosition-leftPosition;
				_root.animate({left: "-="+step+"px"}, _options.duration);
			}
			return false;
		});
		_leftArrow.unbind('click').click(function(){
			var leftPosition = Math.abs(parseInt(_root.css("left")));
			if(leftPosition >= _width && !_root.queue().length) {
				var step = _width*_options.step;
				if(leftPosition <= _width*_options.step) _leftArrow.removeClass("active");
				if(leftPosition-step <= _maxPosition) _rightArrow.addClass("active");
				if(leftPosition < step) step = leftPosition;
				_root.animate({left: "+="+step+"px"}, _options.duration);
			}
			return false;
		});
		
		jQuery(window).unbind('resize').resize(function() {
      _visibleWidth = parseInt(_root.parent().outerWidth());
		  if(_length < _visibleWidth/_width) return;
      _maxPosition = _length*_width-_visibleWidth;
      var leftPosition = Math.abs(parseInt(_root.css("left"))); 
			if(leftPosition > _maxPosition) _root.css('left', (-_maxPosition)+'px');
    });
  });
}