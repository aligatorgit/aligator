<?php

function allegro_item_array($item){
  return is_array($item) ? $item : array($item);
}

function allegro_left_block($block = NULL){
  static $allegro_block;
  
  if(!is_null($block)) $allegro_block = $block;
  
  return !empty($allegro_block) ? $allegro_block : '';
}

function allegro_right_block($block = NULL){
  static $allegro_block;
  
  if(!is_null($block)) $allegro_block = $block;
  
  return !empty($allegro_block) ? $allegro_block : '';
}

function allegro_default($name){
  $vars = array(
    'page' => 0,
    'limit' => 60,
    'condition' => 'all',
    'type' => 'all',
    'from' => 0,
    'to' => 0,
    'sort' => 'endingTime',
    'category' => 620,
    'description' => 0,
    'seller' => '',
  );
  return isset($vars[$name])?$vars[$name]:'';
}

function allegro_search_string(){
  if(arg(0)=='search') return urldecode(arg(1));
  
  $node = menu_get_object('node');
  if($node&&$node->type=='article'&&isset($node->field_category['und'][0]['tid'])) {
    $term = taxonomy_term_load($node->field_category['und'][0]['tid']);
    return isset($term->field_search['und'][0]['value']) ? $term->field_search['und'][0]['value'] : $term->name;
  }
  
  $term = menu_get_object('taxonomy_term', 2);
  if($term&&$term->vid == 1){
    return isset($term->field_search['und'][0]['value']) ? $term->field_search['und'][0]['value'] : $term->name;
  }
  
  return '';
}

function allegro_link($text, $name='', $value=''){
  $active_class = allegro_arg($name)==$value?' class="active"':'';
  return '<a href="'.allegro_url($name, $value).'"'.$active_class.'>'.$text.'</a>';
}

function allegro_url($name='', $value=''){
  $param = $_GET;
  if(allegro_default($name)==$value) unset($param[$name]);
  else $param[$name] = $value;
  
  $path = str_replace(array('%2F', '+'), array('/', '%20'), urlencode(current_path()));
  
  return '/'.$path.allegro_query($param);
}

function allegro_query($param){
  unset($param['q']);
  $query = http_build_query($param, '', '&amp;');
  return $query?'?'.$query:'';
}

function allegro_arg($name, $default_value=null){
  if(is_null($default_value)) $default_value = allegro_default($name);
  return isset($_GET[$name])?$_GET[$name]:$default_value;
}

function allegro_price_filter(){
  $from = allegro_arg('from', '');
  $to = allegro_arg('to', '');
  $output = '<form><input type="text" name="from" value="'.$from.'" placeholder="от" /> - <input type="text" name="to" value="'.$to.'" placeholder="до" /><input type="submit" value="Фильтровать" />';
  foreach($_GET as $name=>$value){
    if(in_array($name, array('q', 'from', 'to'))) continue;
    $output .= '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
  }
  $output .= '</form>';
  return $output;
}

function currency_exchange($value){
  if($value<0) return 0;
  /*$rate = variable_get('allegro_currency');
  return round($value*$rate, 2);*/
  return $value;
}

function allegro_search_log($word){
  if(!isset($_SESSION['allegro_search_log'])) $_SESSION['allegro_search_log'] = array();
  if(in_array($word, $_SESSION['allegro_search_log'])) return;
  $_SESSION['allegro_search_log'][] = $word;
  
  $result = db_select('allegro_search_log', 'l')
    ->fields('l', array('count'))
    ->condition('l.word', $word)
    ->execute()
    ->fetchObject();
    
  if(!$result){
    db_insert('allegro_search_log')
      ->fields(array(
        'word' => $word,
        'count' => 1,
        'request' => REQUEST_TIME,
      ))
      ->execute();
  } else {
    db_update('allegro_search_log')
      ->fields(array(
        'count' => $result->count+1,
        'request' => REQUEST_TIME,
      ))
      ->condition('word', $word)
      ->execute();    
  }
}