<?php
/**
 * @file
 * meta_tags.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function meta_tags_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: cat.
  $config['cat'] = array(
    'instance' => 'cat',
    'config' => array(
      'title' => array(
        'value' => '[current-page:title] | [site:name]',
      ),
    ),
  );

  // Exported Metatag config instance: global.
  $config['global'] = array(
    'instance' => 'global',
    'config' => array(
      'title' => array(
        'value' => '[current-page:title] — [site:name]',
      ),
      'description' => array(
        'value' => '',
      ),
      'abstract' => array(
        'value' => '',
      ),
      'keywords' => array(
        'value' => '',
      ),
      'robots' => array(
        'value' => array(
          'index' => 0,
          'follow' => 0,
          'noindex' => 0,
          'nofollow' => 0,
          'noarchive' => 0,
          'nosnippet' => 0,
          'noodp' => 0,
          'noydir' => 0,
          'noimageindex' => 0,
          'notranslate' => 0,
        ),
      ),
      'news_keywords' => array(
        'value' => '',
      ),
      'standout' => array(
        'value' => '',
      ),
      'generator' => array(
        'value' => '',
      ),
      'rights' => array(
        'value' => '',
      ),
      'image_src' => array(
        'value' => '',
      ),
      'canonical' => array(
        'value' => '',
      ),
      'shortlink' => array(
        'value' => '',
      ),
      'publisher' => array(
        'value' => '',
      ),
      'author' => array(
        'value' => '',
      ),
      'original-source' => array(
        'value' => '',
      ),
      'revisit-after' => array(
        'value' => '',
        'period' => '',
      ),
      'content-language' => array(
        'value' => '',
      ),
    ),
  );

  // Exported Metatag config instance: global:403.
  $config['global:403'] = array(
    'instance' => 'global:403',
    'config' => array(
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: global:404.
  $config['global:404'] = array(
    'instance' => 'global:404',
    'config' => array(
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: global:frontpage.
  $config['global:frontpage'] = array(
    'instance' => 'global:frontpage',
    'config' => array(
      'title' => array(
        'value' => 'Allegator — доставка из Польши в Украину с аукциона Allegro',
      ),
      'description' => array(
        'value' => 'Allegator - 8 лет опыта . Доставка автозапчастей с авторазборок Польши и аукциона Allegro Алегро на русском языке',
      ),
      'keywords' => array(
        'value' => 'Запчасти автозапчасти авторазборки разборки Польша доставка посредник Allegro Алегро Аллегро на Русском',
      ),
      'canonical' => array(
        'value' => '[site:url]',
      ),
      'shortlink' => array(
        'value' => '[site:url]',
      ),
    ),
  );

  // Exported Metatag config instance: itm--.
  $config['itm--'] = array(
    'instance' => 'itm--',
    'config' => array(
      'title' => array(
        'value' => '[current-page:title]  - Allegator доставка из Польши',
      ),
      'description' => array(
        'value' => 'Запчасть [current-page:title] - с авторазборки в Польше . Аукцион Allegro с доставкой',
      ),
      'keywords' => array(
        'value' => 'Запчасти автозапчасти авторазборки разборки Польша доставка посредник Allegro Алегро Аллегро на Русском',
      ),
    ),
  );

  // Exported Metatag config instance: node.
  $config['node'] = array(
    'instance' => 'node',
    'config' => array(
      'title' => array(
        'value' => '[node:title] — [site:name]',
      ),
      'description' => array(
        'value' => '[node:summary]',
      ),
    ),
  );

  // Exported Metatag config instance: node:article.
  $config['node:article'] = array(
    'instance' => 'node:article',
    'config' => array(
      'title' => array(
        'value' => '[node:title] в Польше | Все авторазборки [node:field_category] в Польше и в Украине',
      ),
      'description' => array(
        'value' => 'Автозапчасти [node:field_category]. Поиск запчастей из Польши с аукциона Allegro. Доставка по Украине.',
      ),
    ),
  );

  // Exported Metatag config instance: node:blog.
  $config['node:blog'] = array(
    'instance' => 'node:blog',
    'config' => array(
      'title' => array(
        'value' => '[node:title] — [site:name]',
      ),
    ),
  );

  // Exported Metatag config instance: node:page.
  $config['node:page'] = array(
    'instance' => 'node:page',
    'config' => array(
      'title' => array(
        'value' => '[node:title] — [site:name]',
      ),
    ),
  );

  // Exported Metatag config instance: node:webform.
  $config['node:webform'] = array(
    'instance' => 'node:webform',
    'config' => array(),
  );

  // Exported Metatag config instance: search.
  $config['search'] = array(
    'instance' => 'search',
    'config' => array(
      'title' => array(
        'value' => '[current-page:title] — категории автозапчастей на аукционе Allegro',
      ),
      'description' => array(
        'value' => 'Автозапчасти в категории - [current-page:title] - доставка из Польши с Allegro',
      ),
      'keywords' => array(
        'value' => 'Запчасти автозапчасти авторазборки разборки Польша доставка посредник Allegro Алегро Аллегро на Русском',
      ),
    ),
  );

  // Exported Metatag config instance: taxonomy_term.
  $config['taxonomy_term'] = array(
    'instance' => 'taxonomy_term',
    'config' => array(
      'title' => array(
        'value' => '[term:name] | [site:name]',
      ),
      'description' => array(
        'value' => '[term:description]',
      ),
    ),
  );

  // Exported Metatag config instance: taxonomy_term:auto.
  $config['taxonomy_term:auto'] = array(
    'instance' => 'taxonomy_term:auto',
    'config' => array(
      'title' => array(
        'value' => 'Разборка [term:name] [current-page:domain_case] | Все авторазборки [term:name] в Польше и в Украине',
      ),
      'description' => array(
        'value' => 'Автозапчасти [term:name]. Поиск запчастей из Польши с аукциона Allegro. Доставка по Украине.
',
      ),
      'keywords' => array(
        'value' => 'Разборка [term:parents] [current-page:domain]',
      ),
    ),
  );

  // Exported Metatag config instance: user.
  $config['user'] = array(
    'instance' => 'user',
    'config' => array(
      'title' => array(
        'value' => '[user:name] — [site:name]',
      ),
    ),
  );

  // Exported Metatag config instance: view.
  $config['view'] = array(
    'instance' => 'view',
    'config' => array(
      'title' => array(
        'value' => '[site:slogan]',
      ),
      'description' => array(
        'value' => '[view:description]',
      ),
      'canonical' => array(
        'value' => '[view:url]',
      ),
    ),
  );

  return $config;
}
