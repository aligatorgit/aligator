<div class="filters">
  <?php if($categories): ?>
  <div class="filter">
    <h3>Подкатегории</h3>
    <?php foreach($categories as $row): ?>
        <?php if(!drupal_is_front_page()): ?>
            <?php if(arg(0)=='search'): ?>
            <?php print allegro_link(correct_tpr($row->name).' <small>('.$row->count.')</small>', 'category', $row->id); ?>
            <?php else: ?>
            <a href="/cat/<?php print $row->id.allegro_query($_GET) ?>"><?php print correct_tpr($row->name) ?> <small>(<?php print $row->count ?>)</small></a>
            <?php endif; ?>
        <?php else: ?>
            <a href="/cat/<?php print $row->id.allegro_query($_GET) ?>"><?php print correct_tpr($row->name) ?> <small>(<?php print $row->count ?>)</small></a>
        <?php endif; ?>
    <?php endforeach; ?>
  </div>
  <?php endif; ?>
  <div class="filter">
    <h3>Состояние</h3>
    <?php print allegro_link('Любое', 'condition', 'all'); ?>
    <?php print allegro_link('Новое', 'condition', 'new'); ?>
    <?php print allegro_link('Б/У', 'condition', 'used'); ?>
  </div>
  <div class="filter">
    <h3>Тип предложения</h3>
    <?php print allegro_link('Все', 'type', 'all'); ?>
    <?php print allegro_link('Купить сейчас', 'type', 'buyNow'); ?>
    <?php print allegro_link('Аукцион', 'type', 'auction'); ?>
  </div>
  <div class="filter">
    <h3>Цена</h3>
    <form<?php if(drupal_is_front_page()) print ' action="search"'; ?>>
      <input type="text" name="from" value="<?php print allegro_arg('from', '') ?>" placeholder="от" /> - <input type="text" name="to" value="<?php print allegro_arg('to', '') ?>" placeholder="до" /><input type="submit" value="Фильтровать" />
      <?php
        foreach($_GET as $name=>$value){
          if(in_array($name, array('q', 'from', 'to', 'limit'))) continue;
          print '<input type="hidden" name="'.$name.'" value="'.$value.'" />';
        }
      ?>
    </form>
  </div>
</div>