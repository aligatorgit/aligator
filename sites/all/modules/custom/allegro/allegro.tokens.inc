<?php


/**
 * Implements hook_token_info().
 */
function allegro_token_info() {  
  $info['types']['search'] = array(
    'name' => 'Поиск Allegro',
    'description' => 'Маркеры для поиска на Allegro',
    'needs-data' => 'search',
  );
  $info['tokens']['search']['search'] = array(
    'name' => 'Запрос пользователя',
    'description' => 'Ключевая фраза, которую ввел пользователь',
  );
  $info['tokens']['search']['main-category'] = array(
    'name' => 'Главная категория поиска',
    'description' => 'Главная категория поиска',
  );
  
  $info['types']['category'] = array(
    'name' => 'Категория Allegro',
    'description' => 'Маркеры для категории на Allegro',
    'needs-data' => 'category',
  );
  $info['tokens']['category']['main-category'] = array(
    'name' => 'Название категории',
    'description' => 'Название выбранной категории',
  );
  
  $info['types']['product--'] = array(
    'name' => 'Товар Allegro',
    'description' => 'Маркеры для товара на Allegro',
    'needs-data' => 'product--',
  );
  $info['tokens']['product--']['product'] = array(
    'name' => 'Название товара',
    'description' => 'Название товара',
  );
  $info['tokens']['product--']['main-category'] = array(
    'name' => 'Название категории',
    'description' => 'Название главной категории товара',
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function allegro_tokens($type, $tokens, array $data = array(), array $options = array()) {
  $replacements = array();

  $info = allegro_token_info();
  if(isset($info['types'][$type])){
    foreach ($tokens as $name => $original){
      if(isset($options[$type][$name])) {
        $replacements[$original] = $options[$type][$name];
      }
    }
  }

  return $replacements;
}
