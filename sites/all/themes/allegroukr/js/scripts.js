(function($){
  $(function(){
    if(Drupal.settings.allegroProductLink){
      $('div.product-link').append(Drupal.settings.allegroProductLink);
    }
  
    $('a.show-menu').click(function(){
      $('body').toggleClass('menu-opened');
      return false;
    });
    
    $('.filters .filter h3').click(function(){
      $(this).closest('.filter').toggleClass('opened');
      return false;      
    });
  
    $('a[href="#open"]').click(function(){
      $(this).siblings('div.item-list').find('ul').toggle();
      return false;
    }).parent().filter(':has(a.active)').find('ul').show();
    
    if($('#banner').length) $('#banner').bjqs({
      responsive  : true
    });
    if($('#ad').length) $('#ad').bjqs({
      responsive  : true,
      randomstart   : true
    });
    $('#search form').submit(function(){
      var query = $(this).find('input:hidden').not('[name="q"]').not('[name="in-description"]').not('[name="search"]').serialize();

      if(query.indexOf('description=') == -1){
        var description = parseInt($(this).find('[name="in-description"]').val());
        if(description) query += '&description=1';
      }
      
      if(query) query = '?' + query;
      
      var val = $(this).find("[name='search']").val();
      val = val.replace('\\', '');
      val = val.replace('/', '');
      val = encodeURIComponent(val);
      
      window.location = '/search/' + val + query;
      
      return false;
    })
    $('div.slider-photo .slider').simpleSilder();
  });
  
  Drupal.behaviors.mymodule = {
    attach: function (context, settings) {
      $('a.show-menu').attr('data-count', $('#cart a').attr('data-count'));
      
      $('div.basic-cart-delete').once().click(function(){
        $(this).siblings('div.basic-cart-cart-quantity').find('input').val(0).closest('div.form-item').hide().closest('form').find('input.form-submit').click();
      });
    }
  };
  
  Drupal.behaviors.modal = {
    attach: function (context, settings) {   
      Drupal.settings.CToolsModal = {
        modalTheme: 'customModal',
        throbberTheme: 'CToolsModalThrobber',
        animation: 'fadeIn',
        animationSpeed: 'fast',
        modalSize: {
          type: 'fixed',
          width: 'auto',
          height: 'auto',
          addWidth: 0,
          addHeight: 0,
        },
        modalOptions: {
          opacity: .55,
          background: '#000'
        }
      }; 
    }, 
  };
  
  Drupal.theme.prototype.customModal = function () {
    var html = '';
    html += '  <div id="ctools-modal">'
    html += '    <div class="ctools-modal-content">';
    html += '      <div class="modal-header">';
    html += '        <a class="close" href="#"></a>';
    html += '        <span id="modal-title" class="modal-title">&nbsp;</span>';
    html += '      </div>';
    html += '      <div id="modal-content" class="modal-content">';
    html += '      </div>';
    html += '    </div>';
    html += '  </div>';
    return html;
  }
  
  window.alert = function(message) {
    if(typeof console != 'undefined') console.log(message);
  };
  
  if(Drupal.jsAC) {
    Drupal.jsAC.prototype.select = function (node) {
      var advanced = $(this.input.form).hasClass('advanced-autocomplete');
      
      this.input.value = $(node).data('autocompleteValue');
      if(advanced) $(this.input.form).submit();     
    };
    Drupal.jsAC.prototype.found = function (matches) {
      var advanced = $(this.input.form).hasClass('advanced-autocomplete');
      
      if (!advanced&&!this.input.value.length) {
        return false;
      }
    
      var ul = $('<ul></ul>');
      var ac = this;
      for (key in matches) {
        $('<li></li>')
          .html($('<div></div>').html(matches[key]))
          .mousedown(function () { ac.select(this); })
          .mouseover(function () { ac.highlight(this); })
          .mouseout(function () { ac.unhighlight(this); })
          .data('autocompleteValue', key)
          .appendTo(ul);
      }
      
      if(advanced) {
        var checked = parseInt($(this.input.form).find('input[name="in-description"]').val());
        
        var li = $('<li></li>')
          .addClass('in-description')
          .append($('<input id="in-description" type="checkbox" />').change(function(){ 
            var checked = $(this).is(':checked')?1:0;
            $(this.form).find('input[name="in-description"]').val(checked);
          }))
          .append('Искать в описании товара или по номеру запчасти')
          .mousedown(function () { return false; })
          .appendTo(ul);
        if(checked) li.find('#in-description').attr('checked', 'checked');
        else li.find('#in-description').attr('checked', null);
      }

      if (this.popup) {
        if (ul.children().length) {
          $(this.popup).empty().append(ul).show();
          $(this.ariaLive).html(Drupal.t('Autocomplete popup'));
        }
        else {
          $(this.popup).css({ visibility: 'hidden' });
          this.hidePopup();
        }
      }
    };
  }
})(jQuery);