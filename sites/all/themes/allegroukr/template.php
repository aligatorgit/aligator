<?php
  
function allegroukr_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    foreach($breadcrumb as $k => $row){
      if(strpos($row, '<a') !== FALSE){
        $text = strip_tags($row);
        $row = str_replace($text, '<span itemprop="name">' . $text . '</span>', $row);
        $row = str_replace('<a', '<a itemprop="item"', $row);
        
        $breadcrumb[$k] = '';
        $breadcrumb[$k] .= '<span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">'; 
        $breadcrumb[$k] .= $row; 
        $breadcrumb[$k] .= '<meta itemprop="position" content="' . ($k+1) . '" />';
        $breadcrumb[$k] .= '</span>';
      }
    }
    return '<div class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">' . implode(' » ', $breadcrumb) . '</div>';
  }
}