<?php

function allegro_autocomplete($string){
  $result = gethtml('http://allegro.pl/Suggest/Index.php/suggestAjax?q='.$string);
  
  if(!$result) drupal_json_output(array());
  $result = json_decode($result);
  
  $matches = array();
  foreach ($result as $row) {
    $row = json_decode($row);
    $row->phrase = stripslashes($row->phrase);
    
    $matches[strip_tags($row->phrase)] = $row->phrase;
  }
 
  drupal_json_output($matches);
}