<?php
/**
 * @file
 * Basic cart shopping cart block
 */
?>
<div id="cart" class="bottom-shadow">
<?php if (empty($cart)): ?>
  <a>Ваша корзина пуста</a>
<?php else: ?>
  <?php 
    $count = 0;
    foreach($cart as $product){
      $count += $product->basic_cart_quantity;
    }
  ?>
  <a href="/cart" data-count="<?php print $count ?>">В корзине <b><?php print $count ?></b> <?php 
    $mod = $count%10;
    $dec = floor($count/10)%10;
    if($mod == 1 && $dec != 1){
      print 'товар';
    } else if(($mod == 2 || $mod == 3 || $mod == 4) && $dec != 1){
      print 'товара';
    } else {
      print 'товаров';
    }
  ?></a>
<?php endif; ?>
</div>