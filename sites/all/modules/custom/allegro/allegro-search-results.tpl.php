<?php if (!$results): ?>
<p class="search-no-result">
  К сожалению по данному запросу ничего не найдено.
</p>
<?php else: ?>
<?php foreach ($results as $k=>$item): ?>
<div class="search-result row-<?php print $k ?>" itemscope itemtype="http://schema.org/Product">
  <a target="_blank" href="/product/<?php print $item->id ?>" class="clearfix">
    <?php if (!empty($item->images)) foreach (allegro_item_array($item->images) as $image): ?>
      <span class="image">
        <img 
          itemprop="image" 
          src="<?php print str_replace('/original/', '/s160/', $image->url) ?>" 
          alt="<?php print correct_tpr($item->name) ?>" 
          title="<?php print correct_tpr($item->name) ?>" 
          width="128" 
          height="96" />
      </span>
      <?php break; ?>
    <?php endforeach; ?>
    <span class="title" itemprop="name"><?php print correct_tpr($item->name) ?></span>
    <span class="subtitle">(На польском: <?php print $item->name ?>)</span>
    <span class="number">Номер товара: <b><?php print $item->id ?></b></span>
    <span class="price-block" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <?php if ($item->sellingMode->format == 'BUY_NOW' || !empty($item->sellingMode->fixedPrice)): ?>
      <span class="price"><span itemprop="price"><?php 
        if (!empty($item->sellingMode->fixedPrice)) {
          print currency_exchange($item->sellingMode->fixedPrice->amount);
        }
        else {
          print currency_exchange($item->sellingMode->price->amount);
        }
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></span>
      <?php endif; ?>
      <?php if ($item->sellingMode->format == 'AUCTION'): ?>
      <span class="bid">Текущая ставка: <b><span itemprop="price"><?php 
        print currency_exchange($item->sellingMode->price->amount)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></b></span>
      <?php endif; ?>
    </span>
  </a>
</div>
<?php endforeach; ?>
<?php endif; ?>