<?php if($similar && $item->itEndingInfo != 1): ?>
<div class="search-results">
  <div class="search-result-rows">
    <?php print theme('allegro_search_results', array('results' => $similar)); ?>
  </div>
</div>
<?php endif; ?>
<div itemscope itemtype="http://schema.org/Product">
  <h1 itemprop="name"><?php print correct_tpr($item->itName) ?></h1>
  <div class="product-images">
    <div class="image"><a class="fancybox" rel="group" href="<?php print $images['big'][0] ?>?.jpg"><img itemprop="image" src="<?php print $images['medium'][0]; ?>?.jpg" width="400" height="300" alt="<?php print correct_tpr($item->itName) ?>" /></a></div>
    <div class="slider-photo">
        <div class="slider-prev"></div>
        <div class="slider">
          <ul>
            <?php foreach($images['small'] as $k=>$img): ?>
            <?php if(!$k) continue; ?>
            <li><a class="fancybox" rel="group" href="<?php print $images['big'][$k] ?>?.jpg"><img src="<?php print $img; ?>?.jpg" width="128" height="96" alt="<?php print correct_tpr($item->itName) ?>" /></a></li>
            <?php endforeach; ?>
          </ul>
        </div>
        <div class="slider-next"></div>
    </div>
  </div>
  <div class="product-info">
    <div class="product-price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
      <div>Цена в Польше:</div>
      <?php if($item->itBuyNowPrice > 0): ?>
      <div class="price"><span itemprop="price"><?php 
        print currency_exchange($item->itBuyNowPrice)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></div>
      <?php endif; ?>
      <?php if($item->itPrice > 0): ?>
      <div class="bid">Текущая ставка: <b><span itemprop="price"><?php 
        print currency_exchange($item->itPrice)
      ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></b></div>
      <?php endif; ?>
      <?php if($item->itPostDelivery > 0): ?>
      <div class="bid">Доставка в Польше: <b><?php 
        print currency_exchange($item->itPostDelivery);
      ?> PLN</b></div>
      <?php endif; ?>
    </div>
    <div class="product-rate">
      Для уточнений стоимости доставки в Украину и комиссии обратитесь к менеджеру
    </div>
    <?php if($item->itEndingTime): ?>
    <?php if($item->itEndingTime-time() < 0): ?>
    <div class="product-time">Торги завершены</div>
    <?php else: ?>
    <div class="product-time"><b><?php print format_interval($item->itEndingTime-time()) ?></b> — до завершения торгов</div>
    <?php endif; ?>
    <?php endif; ?>
    <?php 
      print allegro_basic_cart_add_to_cart_button(array(
        'nid' => $item->itId
      )); 
    ?>
    <div class="product-condition"><?php 
      foreach($attr as $attribute) {
        if($attribute->attribName == 'Stan') print 'Состояние товара: '.correct_tpr($attribute->attribValues->item);
      }
    ?></div>
    <div class="product-id">Номер товара: <?php print $item->itId ?></div>
    <div class="product-link"></div>
  </div>
  <div class="product-description" itemprop="description">
  <!--
    <div id="google_translate_element"></div>
    <script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'pl', includedLanguages: 'ru', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
    </script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
  -->
    <h3>Описание товара</h3>
    <div class="padding"><?php print preg_replace('#<a.*?>|</a>#is', '', $item->itDescription) ?></div>
  </div>
</div>