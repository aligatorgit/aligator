<?php if($categories): ?>
<div class="filter">
  <h3>Подкатегории</h3>
  <div class="filter-content">
  <?php foreach($categories as $row): ?>
  <?php if(arg(0)=='search'): ?>
  <?php print allegro_link(correct_tpr($row->name).' <small>('.$row->count.')</small>', 'category', $row->id); ?>
  <?php else: ?>
  <a href="/cat/<?php print $row->id.allegro_query($_GET) ?>"><?php print correct_tpr($row->name) ?> <small>(<?php print $row->count ?>)</small></a>
  <?php endif; ?>
  <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>
<div class="filter">
  <h3>Состояние</h3>
  <div class="filter-content">
  <?php print allegro_link('Любое', 'condition', 'all'); ?>
  <?php print allegro_link('Новое', 'condition', 'new'); ?>
  <?php print allegro_link('Б/У', 'condition', 'used'); ?>
  </div>
</div>
<div class="filter">
  <h3>Тип предложения</h3>
  <div class="filter-content">
  <?php print allegro_link('Все', 'type', 'all'); ?>
  <?php print allegro_link('Купить сейчас', 'type', 'buyNow'); ?>
  <?php print allegro_link('Аукцион', 'type', 'auction'); ?>
  </div>
</div>
<div class="filter">
  <h3>Цена</h3>
  <div class="filter-content">
  <?php print allegro_price_filter(); ?>
  </div>
</div>