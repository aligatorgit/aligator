<?php if($similar && $item->itEndingInfo != 1): ?>
<div class="search-results">
  <div class="search-result-rows">
    <?php print theme('allegro_search_results', array('results' => $similar)); ?>
  </div>
</div>
<?php endif; ?>
<div class="product-wrapper" itemscope itemtype="http://schema.org/Product">
    <meta itemprop="name" content="<?php print correct_tpr($item->itName) ?>">
    <div class="product-images">
    <div class="image"><a class="fancybox" rel="group" href="<?php print $images['big'][0] ?>?.jpg"><img itemprop="image" src="<?php print $images['medium'][0]; ?>?.jpg" width="360" height="270" alt="<?php print correct_tpr($item->itName) ?>" data-zoom-image="<?php print $images['big'][0] ?>?.jpg" /></a></div>
    <div class="owl-carousel">
      <?php foreach($images['small'] as $k=>$img): ?>
      <?php if(!$k) continue; ?>
      <div class="item"><a class="fancybox" rel="group" href="<?php print $images['big'][$k] ?>?.jpg"><img src="<?php print $img; ?>?.jpg" width="128" height="96" alt="<?php print correct_tpr($item->itName) ?>" /></a></div>
      <?php endforeach; ?>
    </div>
  </div>
  <div class="product-info">
    <div class="clearfix">
      <?php     ctools_include('ajax');
      ctools_include('modal');
      ctools_modal_add_js(); ?>
        <div class="contact-link">
          <?php $output = '<div id="magical-modal-link">' . l("Связаться с менеджером", 'contact/nojs', array('attributes' => array('class' => 'ctools-use-modal callback')));
          print $output;
          ?>
        </div>

<!--    <a href="javascript:jivo_api.open();" class="callback">Связаться с менеджером</a>-->
    <?php if($item->itEndingTime): ?>
    <?php if($item->itEndingTime-time() < 0): ?>
    <div class="time">Торги завершены</div>
    <?php else: ?>
    <div class="time"><b>Действителен до:</b> <?php print format_date($item->itEndingTime, 'custom', 'd.m.Y H:i') ?></div>
    <?php endif; ?>
    <?php endif; ?>
    </div>
    
    <div class="info clearfix">
      <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
        <span itemprop="availability" content="http://schema.org/InStock"></span>
        <span itemprop="url" content="<?php print url('/product/' . $item->itId, array('absolute' => true)); ?>"></span>
        <?php if(isset($item->endingTime)): ?>
        <span itemprop="priceValidUntil" content="<?php print $item->endingTime ?>"></span>
        <?php endif ?>
        <?php if($item->itBuyNowPrice > 0): ?>
        <div class="price">Цена: <b><span itemprop="price"><?php 
          print currency_exchange($item->itBuyNowPrice)
        ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></b></div>
        <?php endif; ?>
        <?php if($item->itPrice > 0): ?>
        <div class="bid">Текущая ставка: <b><span itemprop="price"><?php 
          print currency_exchange($item->itPrice)
        ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span></b></div>
        <?php endif; ?>
      </div>
      <div class="condition"><?php
        foreach($attr as $attribute) {
          if($attribute->attribName == 'Stan') print 'Состояние: <b>' . correct_tpr($attribute->attribValues->item) . '</b>';
        }
      ?></div>
      <div class="buy-form">
        <?php if(!empty($item->itQuantity)): ?>
        <span class="qty">
          <input type="text" value="1" data-max="<?php print $item->itQuantity ?>" class="form-text" /> 
          из <b><?php print $item->itQuantity ?></b>
        </span>
        <?php endif; ?>
        <a href="/cart/add/<?php print $item->itId ?>" data-itm="<?php print $item->itId ?>" class="basic-cart-add-to-cart-link add-to-cart">Добавить в корзину</a>
      </div>
    </div>
    
    <div class="delivery">
      <b>Информация о доставке</b>
      <ul>
        <?php if($item->itPostDelivery > 0): ?>
        <li class="bid">Доставка в Польше: <b><?php 
          print currency_exchange($item->itPostDelivery);
        ?> PLN</b></li>
        <?php endif; ?>
        <li>
          Доставка в Украину: <b>уточняйте у менеджера</b>
        </li>
      </ul>
    </div>
    
    <div class="seller">
      <b>Продавец:</b> <a href="<?php print url('search', array('query' => array('seller' => $item->itSellerId))) ?>"><?php print $item->itSellerLogin; ?></a> <small>(<?php print $item->itSellerRating ?>)</small>
      <ul><li><a href="<?php print url('search', array('query' => array('seller' => $item->itSellerId))) ?>">Все товары продавца</a></li></ul>
    </div>
  </div>
  <div class="product-description">
    <meta itemprop="description" content="<?php print 'Запчасть '. correct_tpr($item->itName) .' - с авторазборки в Польше . Аукцион Allegro с доставкой' ?>">
    <h3>Описание товара</h3>
    <div class="padding">
      <?php 
        //$tags_free_description = strip_tags($item->itDescription));
        //var_dump($tags_free_description);
        print preg_replace('#<a.*?>|</a>#is', '', $item->itDescription);
        //$&$-begin
        //$item->itDescription = correct_tpr($item->itDescription, true);
        /*print preg_replace('#<a.*?>|</a>#is', '', $item->itDescription);*/
        //$&$-end
      ?>
    </div>
  </div>
  <?php if($similar && $item->itEndingInfo == 1): ?>
  <?php print theme('allegro_search_owl', array('results' => $similar)); ?>
  <?php endif; ?>
</div>