<?php

function allegro_avtorazborka_list($variables){
  $output = theme('item_list', array(
    'items' => $variables['items'],
    'attributes' => $variables['attributes'],
  ));
  $output = str_replace(array('<div class="item-list">', '</div>'), '', $output);
  return '<div class="filters"><h3>' . $variables['title'] . '</h3><div class="filter">' . $output . '</div></div>';
}

function allegro_avtorazborka_regions(){
  $regions = array(
    'simferopol' => 'Симферополь',
    'vynnytsa' => 'Винница',
    'lutsk' => 'Луцк',
    'dnepropetrovsk' => 'Днепропетровск',
    'donetsk' => 'Донецк',
    'zhitomir' => 'Житомир',
    'uzhgorod' => 'Ужгород',
    'zaporozhe' => 'Запорожье',
    'ivano-Frankovsk' => 'Ивано-Франковск',
    'kiyev' => 'Киев',
    'kirovograd' => 'Кировоград',
    'lugansk' => 'Луганск',
    'lvov' => 'Львов',
    'nikolayev' => 'Николаев', 
    'odessa' => 'Одесса',
    'poltava' => 'Полтава',
    'rovno' => 'Ровно',
    'sumy' => 'Сумы',
    'ternopol' => 'Тернополь',
    'kharikov' => 'Харьков',
    'kherson' => 'Херсон',
    'khmelnitskiy' => 'Хмельницкий',
    'cherkassy' => 'Черкассы',
    'chernigov' => 'Чернигов',
    'chernovtsy' => 'Черновцы',
  );
  
  return $regions;
}

function allegro_avtorazborka_get_region_path($path, $region=null){
  global $path_region;
  if(!$region) $region = $path_region;
  
  $path = drupal_get_path_alias($path);
  $args = explode('/', $path);
  if(($args[0] != 'avtorazborka') && ($args[0] || $args[1] != 'avtorazborka')) return;
  
  $regions = allegro_avtorazborka_regions();
  $last = array_pop($args);
  
  if(isset($regions[$last])) $path = implode('/', $args);
  if($region) $path .= '/' . $region;

  //$&$ - begin   !!! --- ломает ссылки на регионы в https://allegator.com.ua/avtorazborka
  // в https://allegator.com.ua/avtorazborka/audi ссылки в порядке,
  // но после перехода по https://allegator.com.ua/avtorazborka/khmelnitskiy/audi
  // результат поиска не по региону и не по марке
  // не применимо (current file - 
  // $links[$parent]['data'] = l($links[$parent]['data']['title'], allegro_avtorazborka_get_region_path('taxonomy/term/'.$parent));)
  // вносить изменения в вызовах
  /*if($region){
    $start_pos = strpos($path, '/');
    $path = substr_replace($path, '/'.$region, $start_pos, 0);
  }*/

  /*echo 'path returned';
  var_dump($path);
  echo '<br><br><br>';*/
  //$&$ - end
  
  return $path;
}

function allegro_avtorazborka_category_region(){ // 1 usage
  $regions = allegro_avtorazborka_regions();
  
  $links = array();
  foreach($regions as $region=>$title){
    $region_path = allegro_avtorazborka_get_region_path(current_path(), $region);
    //$&$ - begin
    $path_arr = explode('/', $region_path);
    $region_str = $path_arr[count($path_arr) - 1];
    $region_arr = array($region_str);
    
    if(array_key_exists($region_str, $regions)){

      array_splice($path_arr, 1, 0, $region_arr);
      array_pop($path_arr);
      $test_region_path = implode('/', $path_arr); // replace $region_path with it
      $links[$region]['data'] = l('Разборка '.$title, $test_region_path);
    //$&$ - end
    } else { //$&$ - condition added
      $links[$region]['data'] = l('Разборка '.$title, $region_path);
    }
  }
  
  return allegro_avtorazborka_list(array(
    'title' => 'Каталог Авторазборок по регионам',
    'items' => $links,
    'attributes' => array('class' => array('clearfix')),
  ));
}

function allegro_avtorazborka_category_auto_links(){
  if($cached = cache_get('allegro:category:auto', 'cache')){
    $links = $cached->data;
  } else {
    $links = array();
    $terms = taxonomy_get_tree(1, 0, 2);
    foreach($terms as $term){
      $parent = $term->parents[0];
      $term = taxonomy_term_load($term->tid);
      $class = array();
      if($parent){
        $links[$parent]['children'][] = array(
          'title' => $term->title, 
          'tid' => $term->tid,
        );
      } else {
        $links[$term->tid]['data'] = array(
          'title' => $term->title,
        );
      }
    }
    cache_set('allegro:category:auto', $links, 'cache', time() + 7*24*60*60); 
  }
  
  return $links;
}

function allegro_avtorazborka_category_auto(){
  global $is_avtorazborka;
  $regions = allegro_avtorazborka_regions();
  
  if(!$is_avtorazborka) return;
  
  $links = allegro_avtorazborka_category_auto_links();

  $node = menu_get_object('node');
  $term = menu_get_object('taxonomy_term', 2);

  foreach($links as $parent=>$children){
    $active = $term&&$parent==$term->tid;

    $region_path = allegro_avtorazborka_get_region_path('taxonomy/term/'.$parent);

    //$&$ - begin
    $path_arr = explode('/', $region_path);
    $region_str = $path_arr[count($path_arr) - 1];
    $region_arr = array($region_str);
    
    if(array_key_exists($region_str, $regions)){

      array_splice($path_arr, 1, 0, $region_arr);
      array_pop($path_arr);
      $test_region_path = implode('/', $path_arr); // replace $region_path with it
      $links[$parent]['data'] = l($links[$parent]['data']['title'], $test_region_path);
    //$&$ - end
    } else { //$&$ - condition added
      $links[$parent]['data'] = l($links[$parent]['data']['title'], $region_path);
    }


    if(!empty($children['children'])){
      foreach($children['children'] as $k=>$child){
        $tid = $links[$parent]['children'][$k]['tid'];
        $title = $links[$parent]['children'][$k]['title'];
        $active |= $tid_active = ($node&&$tid==@$node->field_category['und'][0]['tid']) || ($term&&$tid==$term->tid);
        
        $child_region_path = allegro_avtorazborka_get_region_path('taxonomy/term/'.$tid);
        //$&$ - begin
        $child_path_arr = explode('/', $child_region_path);
        $child_region_str = $child_path_arr[count($child_path_arr) - 1];
        $child_region_arr = array($child_region_str);
    
        if(array_key_exists($child_region_str, $regions)){

          array_splice($child_path_arr, 1, 0, $child_region_arr);
          array_pop($child_path_arr);
          $test_child_region_path = implode('/', $child_path_arr); // replace $child_region_path with it

          $links[$parent]['children'][$k] = array(
            'data' => l($title, $test_child_region_path),
          );
        //$&$ - end
        } else { //$&$ - condition added
          $links[$parent]['children'][$k] = array(
            'data' => l($title, $child_region_path),
          );
        }

        if($tid_active) $links[$parent]['children'][$k]['class'] = array('active');
      }
    }
    if($active) $links[$parent]['class'] = array('active');
  }
  
  return allegro_avtorazborka_list(array(
    'title' => 'Марка и модель авто',
    'items' => $links,
    'attributes' => array('class' => array('clearfix')),
  ));
}

function allegro_avtorazborka_category_allegro($search = null){
  //echo 'allegro_avtorazborka_category_allegro<br>';
  global $is_avtorazborka;

  if(!$is_avtorazborka && is_null($search)) return;
  
  if(is_null($search)) $search = allegro_search_string();
  $search = urlencode($search);
  
  //$&$-commented (translate_correct_tpr & prepare_correct_tpr aren't invoked when cached) if($cached = cache_get('allegro:category:allegro:' . $search, 'cache')){
  if(false){
    $links = $cached->data;
    //echo 'allegro_avtorazborka_category_allegro - links:<br>';
    //var_dump($links);
    //echo '<br><br><br>';
  } else {  
    require_once(drupal_get_path('module', 'allegro').'/allegro.class.inc');
    $allegro = new AllegroMiddleware();
    //Делаем запрос к аллегро
    $results = $allegro->search($search, 620, 0, 18);
    
    //Получаем категории
    $categories = array();
    if (!empty($results->categories->subcategories)) {
      foreach (allegro_item_array($results->categories->subcategories) as $row) {
        prepare_correct_tpr($row->name);
        
        $categories[] = (object)array(
          'id' => $row->id,
          'name' => $row->name,
          'count' => $row->count,
        );
      }
    }
    
    //Получаем результаты
    $items = !empty($results->items) ? array_merge($results->items->promoted, $results->items->regular) : array();
    foreach($items as $item){
      prepare_correct_tpr($item->name);
    }
  
    //Запускаем переводы
    translate_correct_tpr();
    
    $links = array();
    foreach($categories as $row){
      if($search){
        $links[$row->id]['data'] = l(correct_tpr($row->name).' <small>('.$row->count.')</small>', 'search/'.$search, array(
          'html' => true,
          'query' => array(
            'category' => $row->id,
          ),
        ));
      } else{
        $links[$row->id]['data'] = l(correct_tpr($row->name).' <small>('.$row->count.')</small>', 'category/'.$row->id, array('html' => true));
      }
    }
    
    cache_set('allegro:category:allegro:' . $search, $links, 'cache', time() + 7*24*60*60);
    cache_set('allegro:product:allegro:' . $search, $items, 'cache', time() + 7*24*60*60);
  }
  
  return allegro_avtorazborka_list(array(
    'title' => 'Категории Allegro',
    'items' => $links,
    'attributes' => array('class' => array('clearfix')),
  ));
}

function allegro_avtorazborka_taxonomy_term_page($term){
  $build = taxonomy_term_page($term);
    
  if($terms = taxonomy_get_children($term->tid)){
    unset($build['no_content'], $build['nodes'], $build['pager']);
    $terms[$term->tid] = $term;
    $tids = array_keys($terms);
    
    $query = db_select('taxonomy_index', 't');
    $query->addTag('node_access');
    $query->condition('tid', $tids, 'IN');
    $count_query = clone $query;
    $count_query->addExpression('COUNT(t.nid)');
    $query = $query->extend('PagerDefault');
    $query = $query->limit(variable_get('default_nodes_main', 10));
    $query->setCountQuery($count_query);
    $query->addField('t', 'nid');
    $query->addField('t', 'tid');
    $order = array('t.sticky' => 'DESC', 't.created' => 'DESC');
    foreach ($order as $field => $direction) {
      $query->orderBy($field, $direction);
      list($table_alias, $name) = explode('.', $field);
      $query->addField($table_alias, $name);
    }
    if($nids = $query->execute()->fetchCol()){
      $nodes = node_load_multiple($nids);
      $build += node_view_multiple($nodes);
      $build['pager'] = array(
        '#theme' => 'pager',
        '#weight' => 5,
      );
    }
    else {
      $build['no_content'] = array(
        '#prefix' => '<p>',
        '#markup' => t('There is currently no content classified with this term.'),
        '#suffix' => '</p>',
      );
    }
  } 

  return $build;
}