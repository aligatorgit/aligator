<?php

function allegro_spares_form($form, &$form_state){  
  $form['make'] = array(
    '#type' => 'select',
    '#options' => array(
      '' => '- Марка автомобиля -',
    ),
    '#ajax' => array(
      'callback' => 'allegro_spares_form_make_ajax',
    ),
  );
  
  $form['model'] = array(
    '#type' => 'select',
    '#disabled' => TRUE,
    '#options' => array(
      '' => '- Модель автомобиля -',
    ),
    '#prefix' => '<div class="model-wrapper">',
    '#suffix' => '</div>',
  );
  
  $form['class'] = array(
    '#type' => 'select',
    '#options' => array(
      '' => '- Категория детали -',
    ),
    '#ajax' => array(
      'callback' => 'allegro_spares_form_class_ajax',
    ),
  );
  
  $form['type'] = array(
    '#type' => 'select',
    '#disabled' => TRUE,
    '#options' => array(
      '' => '- Тип детали -',
    ),
    '#ajax' => array(
      'callback' => 'allegro_spares_form_type_ajax',
    ),
    '#prefix' => '<div class="type-wrapper">',
    '#suffix' => '</div>',
  );
  
  $form['subtype'] = array(
    '#type' => 'select',
    '#disabled' => TRUE,
    '#options' => array(
      '' => '- Подтип детали -',
    ),
    '#prefix' => '<div class="subtype-wrapper">',
    '#suffix' => '</div>',
  );
  
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => 'Искать',
  );
  
  foreach($_GET as $k=>$v){
    if(isset($form[$k]) && !isset($form_state['values'][$k])) {
      $form_state['values'][$k] = $v;
      $form[$k]['#default_value'] = $v;
    }
  }
  
  $form['make']['#options'] += allegro_auto_get();
  if(!empty($form_state['values']['make'])){
    $form['model']['#options'] += allegro_auto_get($form_state['values']['make']);
    $form['model']['#type'] = 'select';
    $form['model']['#disabled'] = FALSE;
  }
  
  $form['class']['#options'] += allegro_spares_get();
  if(!empty($form_state['values']['class'])){
    if($options = allegro_spares_get($form_state['values']['class'])){
      $form['type']['#options'] += $options;
      $form['type']['#type'] = 'select';
      $form['type']['#disabled'] = FALSE;
    }
    if(!empty($form_state['values']['type'])){
      if($options = allegro_spares_get($form_state['values']['class'], $form_state['values']['type'])){
        $form['subtype']['#options'] += $options;
        $form['subtype']['#type'] = 'select';  
        $form['subtype']['#disabled'] = FALSE;      
      }
    }
  }
  
  return $form;
}

function allegro_spares_form_submit($form, &$form_state){
  $search = array();
  if(!empty($form_state['values']['make'])) $search['make'] = $form_state['values']['make'];
  if(!empty($form_state['values']['model'])) $search['model'] = $form_state['values']['model'];
  
  if(!empty($form_state['values']['class'])) $search['class'] = $form_state['values']['class'];
  if(!empty($form_state['values']['type'])) $search['type'] = $form_state['values']['type'];
  if(!empty($form_state['values']['subtype'])) $search['subtype'] = $form_state['values']['subtype'];

  drupal_goto('search', array(
    'query' => $search,
  ));
}

function allegro_spares_form_make_ajax($form, &$form_state){
  $commands = array();
  $commands[] = ajax_command_replace('form.allegro-spares-form div.model-wrapper', render($form['model']));
  return array('#type' => 'ajax', '#commands' => $commands);
}

function allegro_spares_form_class_ajax($form, &$form_state){
  $commands = array();
  $commands[] = ajax_command_replace('form.allegro-spares-form div.type-wrapper', render($form['type']));
  $commands[] = ajax_command_replace('form.allegro-spares-form div.subtype-wrapper', render($form['subtype']));
  return array('#type' => 'ajax', '#commands' => $commands);
}

function allegro_spares_form_type_ajax($form, &$form_state){
  $commands = array();
  $commands[] = ajax_command_replace('form.allegro-spares-form div.subtype-wrapper', render($form['subtype']));
  return array('#type' => 'ajax', '#commands' => $commands);
}