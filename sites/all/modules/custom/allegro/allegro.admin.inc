<?php

function allegro_settings_code() {
  require_once(drupal_get_path('module', 'allegro') . '/allegro.class.inc');
  $allegro = new AllegroMiddleware();
  
  $tokens = $allegro->generateToken($_GET['code']);
  
  if (isset($tokens->error)) {
    drupal_set_message($tokens->error_description, 'error');
  }
  
  drupal_goto('admin/config/system/allegro');
}

function allegro_settings_form($form, &$form_state){
  require_once(drupal_get_path('module', 'allegro') . '/allegro.class.inc');
  $allegro = new AllegroMiddleware();
  
  $form['allegro_currency'] = array(
    '#type' => 'textfield',
    '#title' => 'Курс Польского Злотого',
    '#states' => array(
      'disabled' => array(
        '[name="allegro_currency_auto"]' => array('checked' => true),
      ),
    ),
    '#default_value' => variable_get('allegro_currency'),
  );
  $form['allegro_currency_auto'] = array(
    '#type' => 'checkbox',
    '#title' => 'Получать курс автоматически',
    '#default_value' => variable_get('allegro_currency_auto', 0),
  );
  $form['allegro_yandex'] = array(
    '#type' => 'textfield',
    '#title' => 'Ключ для Yandex Translate API',
    '#default_value' => variable_get('allegro_yandex'),
    '#description' => 'Получить ключ: ' . l('https://tech.yandex.ru/keys/get/?service=trnsl', 'https://tech.yandex.ru/keys/get/?service=trnsl'),
  );
  $form['allegro_login'] = array(
    '#type' => 'textfield',
    '#title' => 'Логин на Allegro',
    '#default_value' => variable_get('allegro_login'),
  );
  $form['allegro_password'] = array(
    '#type' => 'textfield',
    '#title' => 'Пароль на Allegro',
    '#default_value' => variable_get('allegro_password'),
  );
  $form['allegro_key'] = array(
    '#type' => 'textfield',
    '#title' => 'Ключ для Allegro API',
    '#default_value' => variable_get('allegro_key'),
  );
  $form['allegro_session'] = array(
    '#markup' => 'Текущий ID сессии: '.variable_get('allegro_session', '-'),
  );
  
  $form['allegro_client_id'] = array(
    '#type' => 'textfield',
    '#title' => 'Client ID',
    '#default_value' => variable_get('allegro_client_id'),
  );
  $form['allegro_client_secret'] = array(
    '#type' => 'textfield',
    '#title' => 'Client Secret',
    '#default_value' => variable_get('allegro_client_secret'),
  );
  $form['allegro_access_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Access Token',
    '#default_value' => variable_get('allegro_access_token'),
    '#description' => l('Обновить код', $allegro->getAuthLink()),
    '#disabled' => TRUE,
  );
  $form['allegro_refresh_token'] = array(
    '#type' => 'textfield',
    '#title' => 'Refresh Token',
    '#default_value' => variable_get('allegro_refresh_token'),
    '#disabled' => TRUE,
  );
  $form = system_settings_form($form);
  
  $form['#submit'][] = 'allegro_cron';
  
  return $form;
}

function allegro_translate_seek_screen($lng) {
  if(!in_array($lng, array('rp', 'pr'))) return;
  drupal_add_css(drupal_get_path('module', 'locale') . '/locale.css');

  $elements = drupal_get_form('allegro_translation_filter_form', $lng);
  $output = '<ul class="action-links"><li><a href="/admin/config/regional/allegro/'.$lng.'/add">Добавить перевод</a></li></ul>';
  $output .= drupal_render($elements);
  $output .= _allegro_translate_seek($lng);
  return $output;
}

function allegro_translation_filter_form($form, &$form_state, $lng) {
  if(!in_array($lng, array('rp', 'pr'))) return;

  $form['lng'] = array(
    '#type' => 'value',
    '#value' => $lng,
  );
  $form['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t('Filter translatable strings'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['filters']['string'] = array(
    '#type' => 'textfield',
    '#title' => t('String contains'),
    '#description' => t('Leave blank to show all strings. The search is case sensitive.'),
  );
  $form['filters']['actions'] = array(
    '#type' => 'actions',
    '#attributes' => array('class' => array('container-inline')),
  );
  $form['filters']['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Filter'),
  );
  if (!empty($_SESSION['allegro_translation_filter_'.$lng])) {
    $form['filters']['actions']['reset'] = array(
      '#type' => 'submit',
      '#value' => t('Reset')
    );
    $form['filters']['string']['#default_value'] = $_SESSION['allegro_translation_filter_'.$lng]['string'];
  }
  
  $form['bulk'] = array(
    '#type' => 'fieldset',
    '#title' => t('Bulk upload'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#description' => '<div class="decription"><b>Загрузка файлов .csv</b>: внутри файла необходимо указать польское слово, затем точку с запятой, затем русское слово. Каждый перевод должен выполняться с новой строчки. Кодировка файла должна быть UTF-8.</div>',
  );
  $form['bulk']['file'] = array(
    '#type' => 'file',
    '#theme_wrappers' => array(),
    '#suffix' => ' ',
  );
  $form['bulk']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Upload'),
  );

  return $form;
}

function allegro_translation_filter_form_submit($form, &$form_state) {
  $lng = $form_state['values']['lng'];
  $op = $form_state['values']['op'];
  switch ($op) {
    case t('Filter'):
      if($form_state['values']['string']) $_SESSION['allegro_translation_filter_'.$lng]['string'] = $form_state['values']['string'];
      else $_SESSION['allegro_translation_filter_'.$lng] = array();
      break;
    case t('Reset'):
      $_SESSION['allegro_translation_filter_'.$lng] = array();
      break;
    case t('Upload'):
      $file = file_save_upload('file', array('file_validate_extensions' => array('csv')));
      $count = 0;
      if ($file) {
        $filepath = drupal_realpath($file->uri);
        $data = file($filepath);
        foreach($data as $row){
          $row = explode(';', $row);
          if(count($row)<2) continue;
          $pl = trim($row[0]);
          $ru = trim($row[1]);
          if(!$pl||!$ru) continue;
          
          $orig = $lng=='rp'?'ru':'pl';
          db_delete('allegro_translate_'.$lng.'_addition')
            ->condition($orig, $$orig) // $$orig - это значение переменной $ru или $pl
            ->execute(); 
            
          db_insert('allegro_translate_'.$lng.'_addition')
            ->fields(array(
              'ru' => $ru,
              'pl' => $pl,
            ))
            ->execute(); 
          $count++;
        }
      }
      drupal_set_message('Обновлено <em>'.$count.'</em> записей.');
      break;
  }

  $form_state['redirect'] = 'admin/config/regional/allegro/'.$lng;
}

function _allegro_translate_seek($lng) {
  if(!in_array($lng, array('rp', 'pr'))) return;
  
  $output = '';

  if(isset($_SESSION['allegro_translation_filter_'.$lng])&&!empty($_SESSION['allegro_translation_filter_'.$lng])){
    $query = $_SESSION['allegro_translation_filter_'.$lng];
  } else {
    $query = array(
      'string' => '',
    );
  }

  $sql_query = db_select('allegro_translate_'.$lng.'_addition', 't');
  $sql_query->fields('t');

  if($query['string']){
    $condition = db_or()->condition('t.ru', '%' . db_like($query['string']) . '%', 'LIKE');
    $condition->condition('t.pl', '%' . db_like($query['string']) . '%', 'LIKE');
    $sql_query->condition($condition);
  }

  $sql_query = $sql_query->extend('PagerDefault')->limit(50);
  $results = $sql_query->execute();

  $header = array('Русский', 'Польский', array('data' => t('Operations'), 'colspan' => '2'));

  $strings = array();
  foreach ($results as $row) {
    if (!isset($strings[$row->tid])) {
      $strings[$row->tid] = array(
        'ru' => $row->ru,
        'pl' => $row->pl,
      );
    }
  }

  $rows = array();
  foreach ($strings as $tid => $string) {
    $rows[] = array(
      $string['ru'],
      $string['pl'],
      array('data' => l(t('edit'), "admin/config/regional/allegro/{$lng}/edit/{$tid}", array('query' => drupal_get_destination())), 'class' => array('nowrap')),
      array('data' => l(t('delete'), "admin/config/regional/allegro/{$lng}/delete/{$tid}", array('query' => drupal_get_destination())), 'class' => array('nowrap')),
    );
  }

  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No strings available.')));
  $output .= theme('pager');

  return $output;
}


function allegro_translate_edit_form($form, &$form_state, $lng, $tid) {
  if(!in_array($lng, array('rp', 'pr'))) return;
  
  $source = db_query('SELECT * FROM {allegro_translate_'.$lng.'_addition} WHERE tid = :tid', array(':tid' => $tid))->fetchObject();

  $form['lng'] = array(
    '#type'  => 'value',
    '#value' => $lng,
  );
  $form['ru'] = array(
    '#type'  => 'textfield',
    '#title' => 'Русский',
    '#default_value' => $source?$source->ru:'',
  );
  $form['pl'] = array(
    '#type'  => 'textfield',
    '#title' => 'Польский',
    '#default_value' => $source?$source->pl:'',
  );
  $form['tid'] = array(
    '#type'  => 'value',
    '#value' => $source?$source->tid:null,
  );

  $form['actions'] = array(
    '#type' => 'actions'
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit', 
    '#value' => t('Save translations')
  );
  return $form;
}

function allegro_translate_edit_form_submit($form, &$form_state) {
  $tid = $form_state['values']['tid'];
  $lng = $form_state['values']['lng'];
  
  if($tid){
    db_update('allegro_translate_'.$lng.'_addition')
      ->fields(array(
        'ru' => $form_state['values']['ru'],
        'pl' => $form_state['values']['pl'],
      ))
      ->condition('tid', $tid)
      ->execute();
  } else {
    db_insert('allegro_translate_'.$lng.'_addition')
      ->fields(array(
        'ru' => $form_state['values']['ru'],
        'pl' => $form_state['values']['pl'],
      ))
      ->execute();    
  }
    
  drupal_set_message(t('The string has been saved.'));

  $form_state['redirect'] = 'admin/config/regional/allegro/'.$lng;
}


function allegro_translate_delete_page($lng, $tid) {
  if(!in_array($lng, array('rp', 'pr'))) return;
  
  if ($source = db_query('SELECT * FROM {allegro_translate_'.$lng.'_addition} WHERE tid = :tid', array(':tid' => $tid))->fetchObject()) {
    return drupal_get_form('allegro_translate_delete_form', $lng, $source);
  }
  else {
    return drupal_not_found();
  }
}

function allegro_translate_delete_form($form, &$form_state, $lng, $source) {
  if(!in_array($lng, array('rp', 'pr'))) return;
  
  $form['tid'] = array(
    '#type' => 'value', 
    '#value' => $source->tid,
  );
  $form['lng'] = array(
    '#type' => 'value', 
    '#value' => $lng,
  );
  return confirm_form($form, t('Are you sure you want to delete the string "%source"?', array('%source' => $source->ru)), 'admin/config/regional/allegro/'.$lng, t('Deleting the string will remove all translations of this string in all languages. This action cannot be undone.'), t('Delete'), t('Cancel'));
}

function allegro_translate_delete_form_submit($form, &$form_state) {
  $lng = $form_state['values']['lng'];
  
  db_delete('allegro_translate_'.$lng.'_addition')
    ->condition('tid', $form_state['values']['tid'])
    ->execute();
  $form_state['redirect'] = 'admin/config/regional/allegro/'.$lng;
}

function allegro_log_view(){
  $output = '';
  
  $results = db_select('allegro_search_log', 'l')
    ->fields('l', array('word', 'count', 'request'))
    ->orderBy('count', 'DESC')
    ->orderBy('request', 'DESC')
    ->extend('PagerDefault')
    ->limit(50)
    ->execute();
    
  $header = array('Запрос', 'Количество повторений', 'Дата посленего запроса');

  $rows = array();
  foreach ($results as $row) {
    $rows[] = array(
      $row->word,
      $row->count,
      format_date($row->request, 'custom', 'd.m.Y H:i'),
    );
  }

  $form = drupal_get_form('allegro_log_form');

  $output .= render($form);
  $output .= theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('No strings available.')));
  $output .= theme('pager');

  return $output;
}

function allegro_log_form($form, &$form_state) {  
  $form['group'] = array(
    '#type' => 'fieldset',
    '#title' => 'Очистка данных',
    '#description' => '<p>После очистки лога данные будут удалены безвозвратно.</p>', 
  );
  $form['group']['submit'] = array(
    '#type' => 'submit', 
    '#value' => 'Очистка лога',
  );
  return $form;
}

function allegro_log_form_submit($form, &$form_state) {  
  db_delete('allegro_search_log')->execute();
}
