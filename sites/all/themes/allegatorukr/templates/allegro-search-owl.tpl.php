<?php if ($results): ?>
<?php dsm($results); ?>
<div class="view view-recommended-products">
  <h3>Рекомендуемые товары</h3>
  <div class="owl-carousel">
    <?php foreach ($results as $k=>$item): ?>
        <?php if (isset($item->images)): ?>
            <div id="product-<?php print $item->id ?>" class="item" itemscope itemtype="http://schema.org/Product">
              <a href="/itm/<?php print $item->id ?>" class="product clearfix">
                <?php foreach(allegro_item_array($item->images) as $image): ?>
                <span class="image">
                  <img 
                    itemprop="image" 
                    src="<?php print str_replace('/original/', '/s160/', $image->url) ?>" 
                    alt="<?php print correct_tpr($item->name) ?>" 
                    title="<?php print correct_tpr($item->name) ?>" 
                    width="174" 
                    height="144" />
                </span>
                <?php break; ?>
                <?php endforeach; ?>
                <span class="title" itemprop="name"><?php print correct_tpr($item->name) ?></span>
                <meta itemprop="description" content="<?php print 'Запчасть '. correct_tpr($item->name) .' - с авторазборки в Польше . Аукцион Allegro с доставкой' ?>">
                <span class="subtitle">(На польском: <?php print $item->name ?>)</span>
              </a>
              <div class="info">
                <span class="price" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                  <?php if ($item->sellingMode->format == 'BUY_NOW'): ?>
                  <span itemprop="price"><?php 
                    print currency_exchange($item->sellingMode->price->amount)
                  ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span>
                  <?php elseif ($item->sellingMode->format == 'AUCTION'): ?>
                  <span itemprop="price"><?php 
                    print currency_exchange($item->sellingMode->price->amount)
                  ?></span> <span itemprop="priceCurrency" content="PLN">PLN</span>
                  <?php endif; ?>
                </span>
                <?php
                  print allegro_basic_cart_add_to_cart_button(array(
                    'nid' => $item->id
                  ));
                ?>
              </div>
            </div>
        <?php endif; ?>
    <?php endforeach; ?>
  </div>
</div>
<?php endif; ?>