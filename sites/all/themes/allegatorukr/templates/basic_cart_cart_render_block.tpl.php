<?php
/**
 * @file
 * Basic cart shopping cart block
 */
?>
<div id="cart" class="cart">
<?php if (empty($cart)): ?>
  <a>корзина</a>
<?php else: ?>
  <?php 
    $count = 0; $price = 0;
    foreach($cart as $product){
      $count += $product->basic_cart_quantity;
      $price += $product->basic_cart_unit_price * $product->basic_cart_quantity;
    }
  ?>
  <a href="/cart" data-count="<?php print $count ?>"><?php print $price ?> PLN</a>
<?php endif; ?>
</div>