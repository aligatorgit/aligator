<?php

function allegro_auto_get($make = '', $model = '', $translate = FALSE){
  $auto = array(
    'Acura' => array(
      'TSX', 'RDX', 'MDX', 'ZDX',
    ),
    'Alfa Romeo' => array(
      '145', '146', '147', '147GTA', '155', '156GTA', '159', '164', '33', '75', 'Arna', 'Brera', 'GT', 'GTV', 'Mito', 'Speder', 'Giulietta',
    ),
    'Aston Martin' => array( 
      'Vanquish', 'DB7', 'DB9', 'Vantage', 'Rapide',
    ),
    'Audi' => array(
      'A1', 'A2', 'A3', 'A4', 'A5', 'A6', 'A7', 'A8', 'Allroad', 'Cabriolet', 'Coupe', 'Q5', 'Q7', 'Quattro', 'R8', 'RS4', 'RS6', 'S2', 'S3', 'S4', 'S5', 'S6', 'S8', 'TT', 'V8',
    ),
    'Bentley' => array(
      'Continental', 'GT', 'FLYING SPUR',
    ),
    'BMW' => array(
      'E81', 'E82', 'E87', 'E88', 'F20', 'F21', 'F22', 'F23', 'F45', 'F46', 'E46', 'E90', 'E91', 'E93', 'F30', 'F31', 'F34', 'E92', 'F32', 'F33', 'F36', 'E39', 'E60', 'E61', 'F07', 'F10', 'F11', 'E63', 'E64', 'M6', 'F06', 'F12', 'F13', 'E38', 'E65', 'F01', 'F04', 'E31', 'M Roadster', 'M3', 'M4', 'M5', 'M6', 'E60', 'E61', 'E63', 'E64', 'E90', 'F10', 'F12', 'F13', 'F82', 'F83coupe', 'X1', 'X3', 'X4', 'X5', 'X6', 'E53', 'E70', 'E71', 'E83', 'E84', 'F15', 'F16', 'F26', 'F86', 'Z1', 'Z3', 'Z4', 'Z8', 'Z9', 'Z07', 'E85', 'E89', 'Mini Cooper', 'I3', 'I8', 'Alpina',
    ),
    'Cadillac' => array(
      'ATS', 'CTS', 'Escalade', 'SRX', 'STS',
    ), 
    'Chevrolet' => array(
      'Blazer', 'Camaro', 'Сaptiva', 'Cruze', 'Tahoe', 'Epica', 'Evanda', 'Lacetti', 'Orlando', 'Lumina', 'Lachetti', 'Tacuma', 'Volt',
    ),
    'Chrysler' => array(
      '300M', '300C', 'Concorde', 'Crossfire', 'LE Baron', 'LHS', 'PT Cruiser', 'Sebring', 'Vision', 'Voyager', 'Express',
    ),
    'Citroen' => array (
      'Berlingo', 'C-Elysee', 'C1', 'C2', 'C3', 'C3 Picasso', 'C4', 'C4 cactus', 'C4 Picasso', 'C5', 'C6', 'C8', 'DS3', 'DS4', 'DS5', 'Jumper', 'Nemo', 'Xsara', 'Xsara Picasso',
    ),
    'Daewoo' => array(
      'Lanos', 'Matiz', 'Nubira', 'Nexia',
    ),
    'Daihatsu' => array(
      'Sharade', 'Terios', 'Sirion', 'Materia', 'Cuore', 'Feroza', 'Hijet', 'Rocky',   
    ),
    'Dodge' => array(
      'Avenger', 'Caliber', 'Caravan', 'Challenger', 'Charger', 'Dakota', 'Dart', 'Durango', 'Grand', 'Caravan', 'Intrepid', 'Nitro', 'Ram', 'Ram van', 'Stratus', 'Viper',
    ),
    'Ferrari' => array (
      '458 Italia', '599 GTO', '612 Scaglietti', 'California', 'F12', 'F430', 'FF',
    ),
    'Ford' => array(
      'KA', 'Fiesta', 'Focus', 'Focus RS', 'Fusion', 'Kuga', 'Mondeo', 'F-150', 'F-250', 'F-350', 'F-450', 'F-550', 'Mustang', 'C-Max', 'Grand C-Max', 'S-Max', 'Ka', 'Windstar', 'Probe', 'Transit', 'Connect', 'Ranger',
    ),
    'Fiat' => array(
      '500', '500X', 'Albea', 'Barchetta', 'Brava', 'Bravo', 'Cinquecento', 'Croma', 'Fiorino', 'Punto', 'Grande Punto', 'Marea', 'Multipla', 'Palio', 'Panda',  'Skudo', 'Stilo', 'Seicento', 'Ulysse', 'Linea', 'Tipo', 'Ducato',
    ),
    'Honda' => array(
      'Accord', 'Civic', 'Civic Shuttle', 'Civic Type-R', 'Crosstour', 'Concerto', 'CR-V', 'CRX', 'CR-Z', 'HR-V', 'Element', 'Fit', 'Integra', 'Insight', 'Odyssey', 'Jazz', 'Legend', 'Pilot', 'Prelude', 'Shuttle', 'Streem', 'City', 'S2000',
    ),
    'Hummer' => array(
      'H1', 'H2', 'H3',
    ),
    'Hyundai' => array(
      'Accent', 'Veloster', 'Equus', 'SOLARIS', 'Coupe', 'Genesis', 'Grandeur', 'Santa Fe', 'Sonata', 'Tiburon', 'Tucson', 'Getz', 'Terracan', 'Veracruz', 'Atos', 'Matrix', 'Elantra', 'Lantra', 'XG25', 'XG30', 'XG35', 'I10', 'I20', 'I30', 'I40', 'IX35', 'IX55', 'Terracan', 'XG',
    ),
    'Infiniti' => array(
      'G20', 'J30', 'JX', 'M35', 'FX', 'I30', 'I35', 'EX 35', 'EX37', 'FX', 'G35', 'Q40', 'Q45', 'Q50', 'Q60', 'Q70', 'QX4', 'Qx50', 'QX56', 'QX60', 'QX70', 'QX80',
    ),
    'Isuzu' => array(
      'Aska', 'Axiom', 'Gemini', 'Midi', 'Trooper',
    ),
    'Jaguar' => array(
      'S', 'S-Type', 'X-Type', 'XJR', 'X', 'XE', 'XJ6', 'XJ8', 'XK', 'XK8', 'XKR', 'XKR-S', 'XF', 'XFR-S',
    ),
    'Jeep' => array(
      'Cherokee', 'Compass', 'Grand Cherokee', 'Renegade', 'Commander', 'Liberty', 'Patriot', 'Wrangler',
    ),
    'Kia' => array(
      'Carens', 'Carnival', 'Ceed', 'Pro Ceed', 'Cerato', 'Venga', 'Bongo', 'Quoris', 'Magentis', 'Opirus', 'Optima', 'Picanto', 'Pregio', 'Pride', 'Rio', 'Sephia', 'Shuma', 'Sorento', 'Soul', 'Sportage', 'Koup', 'Cerato Koup',  
    ),
    'Lancia' => array(
      'Dedra', 'Delta', 'Kappa', 'Musa', 'Prisma', 'Thema', 'Thesis',
    ),
    'Land Rover' => array(
      'Defender', 'Discovery', 'Freelander', 'Land Rover', 'Range Rover', 'Evoque', 'Vogue',
    ),
    'Lexus' => array(
      'IS', 'IS F', 'RC', 'RC F', 'CT', 'NX', 'HS', 'LF A', 'ES', 'GS', 'GX', 'IS', 'LS', 'LX', 'RX', 'RX 400', 'SC',
    ),
    'Maserati' => array (
      'Coupe', 'Ghibli', 'Grancabrio', 'GranTurismo', 'Quattroporte',
    ),
    'Maybach' => array (
      'S500', 'S600', '57', '62',
    ),
    'Mazda' => array(
      '2', '3', '6', '3 MPS', '6 MPS', 'CX-7', 'CX-5', 'CX-9', 'MX-6', 'MX-3', 'RX-7', 'MPV', 'Premacy', 'RX-7', 'RX-9',
    ),
    'McLaren' => array (
      '650S', 'MP4',
    ),
    'Mercedes' => array(
      'w168', 'w169', 'w176', 'w245', 'w201', 'w202', 'w203', 'w204', 'w208', 'w209', 'w218', 'w219', 'w215', 'w216', 'w210', 'w211', 'w212', 'w207', 'w460', 'w461', 'w463', 'w166', 'w220', 'w221', 'w222', 'w170', 'w171', 'w172', 'w129', 'w230', 'w231', 'A-Class', 'B-Class', 'C-Class',  'CE', 'CLA', 'CLC', 'CLK', 'CLS', 'CL', 'E-Class', 'G-Class', 'GL-Class', 'GLA', 'GLE', 'GLK', 'M-Class', 'ML', 'R', 'S-Class', 'SLK', 'SL', 'SLR', 'SLS AMG Class', 'V', 'VANEO', 'VIANO', 'VARIO', 'VANEO w414' , 'VIANO w638', 'w639', 'w447' , 
    ),
    'Mini' => array (
      'Cooper', 'Countryman', 'One',
    ),
    'Mitsubishi' => array(
      '3000 GT', 'Carisma', 'Colt', 'Eclips', 'Galant', 'Grandis', 'L200', 'L300', 'Lancer', 'Lancer X', 'Lancer Evo', 'Outlander', 'Outlander XL', 'Pajero Wagon' , 'Pejero Sport' , 'Pajero Pinin', 'Space Star', 'Space Wagon', 'Space Runer',
    ),
    'Nissan' => array(
      'Armada', 'Almera', 'Altima', 'Micra', 'Murano', 'Navara', 'Note', 'NP300', 'Pathfinder', 'Patrol', 'Primera', 'Qashqai', 'Quest', 'Skyline', 'Sunny', 'Teana', 'Terrano', 'Tiida', 'X-Trail t30', 'X-trail t31', '350Z', 'Primastar',
    ),
    'Opel' => array(
      'Astra F', 'Astra G', 'Astra H', 'Tigra', 'Vectra С', 'Frontera', 'Insignia', 'Vivaro',
    ),
    'Peugeot' => array(
      '1007', '106', '107', '2008', '205', '206', '207', '208', '3008', '301', '306', '307', '308', '309', '4007', '4008', '405', '406', '407', '408', '5008', '508', '605', '607', '806', '807', 'Boxer', 'Expert', 'Partner', 'RCZ',
    ),
    'Pontiac' => array (
      'Solstice', 'Sunfire', 'Vibe',
    ),
    'Porsche' => array (
      '911', '944', '997', 'Boxster', 'Cayenne', 'Cayman', 'Macan', 'Panamera',
    ),
    'Renault' => array(
      'Clio', 'Clio RS', 'Espace', 'Symbol', 'Laguna', 'Megane', 'Megane RS', 'Sandero', 'Duster', 'Sandero Stepway', 'Fluence', 'Modus', 'Twingo', 'Trafic', 'Scenic', 'Vel Satis', 'Grand Scienic', 'Koleos', 'Grand espace',
    ),
    'Saab' => array(
      '9-2X', '9-7X', '9-3', '9-5', '900', '9000',
    ),
    'Seat' => array(
      'Altea', 'Alhambra', 'Arosa', 'Exeo', 'Freetrack', 'Ibiza', 'Inca', 'Leon', 'Toledo', 'Malaga', 'Terra', 'Cordoba',
    ),
    'Sкoda' => array(
      'Fabia', 'Fabia New', 'Octavia', 'Oktavia Tour', 'Octavia A5', 'Octavia A7', 'Roomster', 'Super B', 'Yeti',   
    ),
    'Smart' => array(
      'Fortwo', 'City', 'Roadster', 'Forfour',
    ),
    'Ssang Yong' => array(
      'Actyon', 'Action Sport', 'Chairman', 'Family', 'Istana', 'Kallista', 'Korando', 'Kyron', 'Musso', 'Rexton', 'Rodius', 'Stavic',
    ),
    'Subaru' => array(
      'BRZ', 'Forester', 'Impreza', 'Justy', 'Legacy', 'Outback', 'Tribeca', 'WRX', 'WRX STI', 'XV',
    ),
    'Suzuki' => array(
      'Grand Vitara', 'Ignis', 'Jimny', 'Kizashi', 'Alto', 'Swift', 'Vitara', 'Liana', 'Splash', 'SX-4',
    ),
    'Tesla' => array(
      'Model S', 'Model X',
    ),
    'Toyota' => array(
      '4Runner', 'Alphard', 'Auris', 'Avensis t25', 't27', 'Avalon', 'Aurion', 'Belta', 'Camry', 'Corolla', 'iQ', 'Innova', 'Isis', 'Kluger Hybrid', 'Urban Cruiser', 'Succeed', 'Raum', 'Fj Cruiser', 'GT86', 'Hiace', 'Land Cruiser 100', 'Land Cruiser 120', 'Land Cruiser 200', 'Land Cruiser 150', 'Marke II', 'Prado', 'Passo', 'Rav4', 'Supra', 'Solara', 'Sequoia', 'Sai', 'Yaris', 'Prius', 'Tacoma', 'Tundra', 'Venza',
    ),
    'Volvo' => array(
      'S60', 'S70', 'S80', 'S90', 'V40 Cross Country', 'V50', 'V60', 'V70', 'XC60', 'XC70', 'XC90',
    ),
    'Volkswagen' => array(
      'Amarok', 'Eos', 'Beetle', 'Bora', 'Crafter', 'Fox', 'Golf V', 'Golf VI', 'Golf VII', ' Golf Plus', 'Golf GTI', 'Caddy', 'Jetta', 'Multivan', 'T4', 'T5', 'Cross Polo', 'Passat b5', 'Passat b6', 'Passat b7', 'Passat CC', 'Phaeton', 'Routan', 'Scirocco', 'Santana', 'Sharan', 'Touareg', 'Touran', 'Tiguan',
    ),
  );
  
  $array = array();
  
  if($make){
    if($model){
      if($translate) return $model;
      $array = array();
    } else {
      if($translate) return $make;
      $array = @$auto[$make];
    }
  } else {
    if($translate) return '';
    $array = array_keys($auto);
  }
  
  return array_combine($array, $array);
}

function allegro_spares_get($category = '', $type = '', $subtype = '', $translate = FALSE){
  $spares = array(
    'Кузовные элементы' => array(
      'translation' => 4094,
      'children' => array(
        'Внешние зеркала' => array(
          'translation' => 'lusterk*',
          'children' => array(
            'Вставки' => 'lusterk*',
            'Комплекты' => 'lusterk*',
            'Корпуса' => 'lusterk*',
          ),
        ),
        'Дверные ограничители' => array(
          'translation' => 'OGRANICZNIK* DRZW*',
          'children' => array(),
        ),
        'Дверные ручки' => array(
          'translation' => 'KLAMk* DRZW*',
          'children' => array(),
        ),
        'Дверные уплотнители' => array(
          'translation' => 'USZCZELK* DRZW*',
          'children' => array(),
        ),
        'Двери . Бампера . Крылья . Крыши ...' => array(
          'translation' => 4096,
          'children' => array(
            'Бампера' => 'ZDERZ*',
            'Двери' => 'DRZW* prz*',
            'Задние панели' => 'pas* tyl*',
            'Капоты' => 'Mask*',
            'Крылья' => 'blotn*',
            'Крыши' => 'dach *',
            'Крышки багажника' => 'klapa tyl*',
            'Решетки' => 'grill*',
            'Передние панели (телевизоры)' => 'pas*',
            'Усилители бамперов' => 'belka',
            'Окуляры' => 'okular*',
            'Пороги' => 'prog*',
            'Четверти' => 'ĆWIARTK*',
            'Рамы' => 'rama',
          ),
        ),
        'Замки,  ключи' => array(
          'translation' => 'zamek*',
          'children' => array(),
        ),
        'Звукоизолирующие элементы' => array(
          'translation' => 'WYGŁUSZEN*',
          'children' => array(),
        ),
        'Кабины' => array(
          'translation' => 'kabin*',
          'children' => array(),
        ),
        'Крепежные элементы (зажимы, направляющие)' => array(
          'translation' => 'ślizg*',
          'children' => array(),
        ),
        'Молдинги' => array(
          'translation' => 249263,
          'children' => array(
            'Бамперные молдинги' => 'LIST* ZDERZ*',
            'Боковые молдинги' => 'LISTWA* DRZW*',
          ),
        ),
        'Петли' => array(
          'translation' => 'ZAWIAS*',
          'children' => array(),
        ),
        'Подкрылки' => array(
          'translation' => 'NADKOL*',
          'children' => array(),
        ),
        'елескопы' => array(
          'translation' => 'ТSIŁOWNI* TELESKOP*',
          'children' => array(),
        ),
        'Релинги' => array(
          'translation' => 'RELING*',
          'children' => array(),
        ),
      ),
    ),
    'Стекла' => array(
      'translation' => 4101,
      'children' => array(
        'Передние' => array(
          'translation' => 'SZYB*',
          'children' => array(),
        ),
        'Боковые Дверные' => array(
          'translation' => 'SZYB*',
          'children' => array(),
        ),
        'Задние' => array(
          'translation' => 'SZYB*',
          'children' => array(),
        ),
        'Кузовные (форточки)' => array(
          'translation' => 'SZYB*',
          'children' => array(),
        ),
        'Люки' => array(
          'translation' => 'szyberd*',
          'children' => array(),
        ),
        'Уплотнения ветрового стекла' => array(
          'translation' => 'USZCZELK* SZYB*',
          'children' => array(),
        ),
      ),
    ),
    'Двигатели и навесное' => array(
      'translation' => 50821,
      'children' => array(
        'Блоки двигателя' => array(
          'translation' => 50823,
          'children' => array(
            'Блоки' => 'blok silnik*',
            'Втулки' => 'PANEWK*',
            'Коленчатые валы' => 'wal korbowy',
            'Маховики' => 'kolo zamachow*',
            'Поршневые кольца' => 'pierscien* tłokow*',
            'Поршни' => 'tlok*',
            'Шкивы' => 'Koło pasow*',
          ),
        ),  
        'Электрическая система двигателя' => array(
          'translation' => 4141,
          'children' => array(
            'Генераторы' => 'Alternat*',
            'Катушки зажигания' => 'cewk* zapł*',
            'Модули зажигания' => 'MODUŁ* zapłon*',
            'Провода зажигания' => 'WIĄZK* zapł*',
            'Стартеры' => 'rozruszn',
          ),
        ),  
        'Впускные коллекторы' => array(
          'translation' => 'KOLEKTOR SSĄC*',
          'children' => array(),
        ),  
        'ГРМ механизмы' => array(
          'translation' => 50842,
          'children' => array(
            'ГРМ Зубчатые ремни и цепи' => 'pasek zębaty lancuch*',
            'Крышки, корпуса' => 'OBUDOWA* ROZRZĄDU',
            'Натяжители и ролики' => 'NAPINACZ* ROZRZĄDU',
            'Распределительные валы' => 'WAl* ROZRZ*',
          ),
        ),  
        'Головки цилиндров' => array(
          'translation' => 50836,
          'children' => array(
            'Головки' => 'GŁOWIC*',
            'Крышки клапанов' => 'POKRYW* ZAWOR*',
            'Прокладки головки блока' => 'USZCZELK* GŁOW*',
          ),
        ),  
        'Двигатели-моторы  в комплекте' => array(
          'translation' => 50849,
          'children' => array(
            'Бензиновые' => 'SILNIK',
            'Дизельные' => 'SILNIK',
          ),
        ),  
        'Клапаны системы рециркуляции ОГ' => array(
          'translation' => 'ZAWÓR* EGR*',
          'children' => array(),
        ),  
        'Опоры двигателя' => array(
          'translation' => 50852,
          'children' => array(
            'Кронштейны крепления' => 'ŁAPA MOCOW*',
            'Подушки двигателя' => 'PODUSZ* silnik*',
          ),
        ),  
        'Крышки двигателя' => array(
          'translation' => 'POKRYW* SILN*',
          'children' => array(),
        ),  
        'Смазка двигателя-элементы' => array(
          'translation' => 50852,
          'children' => array(
            'Датчики давления масла' => 'czujnik cisnien* ole*',
            'Измерители уровня масла, щупы' => 'miark* ole* bagn*',
            'Крышки заливной горловины' => 'Korek Wlew* Ole*',
            'Масляные насосы' => 'pomp* ole*',
            'Поддоны' => 'Misk*',
            'Прокладка поддона' => 'USZCZELK* Misk*',
          ),
        ),  
        'Турбины , турбокомпресоры' => array(
          'translation' => 'Turbosprężark*',
          'children' => array(),
        ),  
        'Шаговые двигатели' => array(
          'translation' => 'SILN* KROK*',
          'children' => array(),
        ),  
        'Элементы привода' => array(
          'translation' => 50832,
          'children' => array(
            'Клиновые ремни' => 'PAS* KLIN*',
            'Натяжители клинового ремня' => 'PAS* KLIN*',
          ),
        ),  
      ),
    ),
    'Трансмиссия-МКПП-АКПП' => array(
      'translation' => 50863,
      'children' => array(  
        'Дифференциалы' => array(
          'translation' => 'DYFER*',
          'children' => array(),
        ), 
        'Карданные валы' => array(
          'translation' => 'WAŁ* NAPĘD*',
          'children' => array(),
        ), 
        'Коробки передач АКПП МКПП' => array(
          'translation' => 50871,
          'children' => array(
            'Автоматические' => 'SKRZYN* BIEGÓW*',
            'Ручные' => 'SKRZYN* BIEGÓW*',
            'Переключатели' => 'mechan* zmian*',
            'Подушки и крепления' => 'Łap* skrzyn* biegów*',
            'Синхронизаторы' => 'Synchroniz*  skrzyn*',
          ),
        ), 
        'Мосты' => array(
          'translation' => 50866,
          'children' => array(
            'Задние' => 'DYFER*',
            'Передние' => 'DYFER*',
          ),
        ), 
        'Полуоси' => array(
          'translation' => 'polos*',
          'children' => array(),
        ), 
        'Сцепление' => array(
          'translation' => 50880,
          'children' => array(
            'Диски' => 'TARCZ* SPRZĘGŁ*',
            'Маховики' => 'kolo zamachow*',
            'Нажимные диски' => 'DOCISK* SPRZĘGŁ*',
            'Насосы и концентрические цилиндры' => 'POMPK* SPRZĘGŁ*',
            'Упорные подшипники' => 'lozysk* oporow*',
          ),
        ), 
        'Шарниры, шрус' => array(
          'translation' => 'PRZEGUB*',
          'children' => array(),
        ),         
      ),
    ),
    'Топливная система' => array(
      'translation' => 18844,
      'children' => array( 
        'Газобалонное оборудование' => array(
          'translation' => 18846,
          'children' => array(
            'Адаптеры, переходники' => 'adapt* lpg',
            'Редукторы' => 'redukt* lpg',
            'Ремонтные комплекты' => 'Zest* Napraw* lpg',
            'Фильтры' => 'filtr* lpg',
          ),
        ), 
        'Бензин, Дизельное топливо' => array(
          'translation' => 18845,
          'children' => array(
            'Топливные насосы' => 'pomp* pal*',
            'Топливные насосы высокого давления' => 'pomp*  WTRYSK*',
            'Форсунки' => 'Wtrysk*',
            'Баки' => 'ZBIORN* paliw*',
            'Датчики уровня топлива' => 'CZUJN* POZIOM* PALIW*',
            'Карбюраторы' => 'PRZEPUSTN*',
            'Крышка бака' => 'KOREK* ZBIORN* bak*',
            'Прокладки ' => 'USZCZELK*',
          ),
        ), 
      ),
    ),
    'Отопление, кондиционирование' => array(
      'translation' => 49183,
      'children' => array(
        'Кондиционирование' => array(
          'translation' => 49184,
          'children' => array(
            'Вентиляторы радиаторов кондиционера' => 'WENTYLATOR CHŁODNIC*',
            'Воздухоохладители' => 'CHŁODNIC*',
            'Компрессоры кондиционирования' => 'SPRĘŻARK* KLIM*',
            'Конденсаторы' => 'kondensator* klim*',
            'Печки' => 'NAGRZEWN*',
            'Патрубки кондиционера' => 'PRZEWÓD* KLIMAT*',
            'Регуляторы и электрические компоненты' => 'REZYSTOR* Klim*',
            'Осушители  и фильтры' => 'Osuszacz* klimat*',
          ),
        ),
        'Отопление, вентиляция' => array(
          'translation' => 49193,
          'children' => array(
            'Венриляторы' => 'WENTYLATOR',
            'Печки' => 'NAGRZEWN*',
            'Резисторы вентилятора' => 'rezyst*',
          ),
        ),       
      ),
    ),
    'Охлаждение двигателя' => array(
      'translation' => 18689,
      'children' => array(
        'Вентиляторы радиаторов' => array(
          'translation' => 'WENTYLATOR* CHŁODN*',
          'children' => array(),
        ),
        'Вискомуфты' => array(
          'translation' => 'wisko*',
          'children' => array(),
        ),
        'Водяные насосы' => array(
          'translation' => 'POMP* WOD*',
          'children' => array(),
        ),
        'Радиаторы' => array(
          'translation' => 'CHŁODNIC*',
          'children' => array(),
        ),
        'Расширительные баки' => array(
          'translation' => 'ZBIORN* WYROWN*',
          'children' => array(),
        ),
        'Термостаты' => array(
          'translation' => 'TERMOSTAT*',
          'children' => array(),
        ),
        'Шланги для радиаторов' => array(
          'translation' => 'WAZ* CHŁODNIC*',
          'children' => array(),
        ),
      ),
    ),
    'Подвеска - ходовая часть' => array(
      'translation' => 8683,
      'children' => array(
        'Амортизаторы' => array(
          'translation' => 8684,
          'children' => array(
            'Передние амортизаторы' => 'AMORTYZ*  prz*',
            'Втулки амортизаторов' => 'PODUSZK* AMORTYZ*',
            'Задние амортизаторы' => 'AMORTYZ*  TYL*',
            'Комплекты амортизаторов' => 'AMORTYZ*',
            'Пыльники и отбойники амортизаторов' => 'OSLON* AMORTYZ*',
          ),
        ),
        'Балки, подрамники' => array(
          'translation' => 250866,
          'children' => array(
            'Балки, подрамники задние' => 'Sanki',
            'Балки, подрамники передние' => 'Sanki',
          ),
        ),
        'Колесные подшипники' => array(
          'translation' => 'ŁOŻYSK* KOŁ*',
          'children' => array(),
        ),
        'Опоры  под пружины' => array(
          'translation' => 'GUM* SPRĘŻYN*',
          'children' => array(),
        ),
        'Поворотные кулаки, ступицы' => array(
          'translation' => 'ZWROTN*',
          'children' => array(),
        ),
        'Пружины подвески' => array(
          'translation' => 'SPRĘŻYN*',
          'children' => array(),
        ),
        'Рессоры и элементы' => array(
          'translation' => 250870,
          'children' => array(
            'Втулки рессор' => 'PANEWK* RESOR*',
            'Листки рессор' => 'RESOR*',
            'Отбойники и скобы рессор' => 'RESOR*',
          ),
        ),
        'Рычаги и элементы' => array(
          'translation' => 250882,
          'children' => array(
            'Втулки, сайлентблоки рычагов' => 'PANEWK*',
            'Рычаги' => 'WAHACZ*',
            'Шаровые опоры' => 'SWORZ* WAHACZ*',
          ),
        ),
        'Стабилизаторы и элементы' => array(
          'translation' => 250875,
          'children' => array(
            'Втулки стабилизаторов' => 'PANEWK* STABILIZ*',
            'Стабилизаторы' => 'STABILIZ*',
            'Стойки стабилизаторов' => 'STABILIZ*',
          ),
        ),
      ),
    ),
    'Тормозная система' => array(
      'translation' => 18834,
      'children' => array(
        'Вакуумный усилитель тормозов' => array(
          'translation' => 'SERW*',
          'children' => array(),
        ),
        'Датчики АБС' => array(
          'translation' => 'CZUJN* ABS*',
          'children' => array(),
        ),
        'Блок ABS' => array(
          'translation' => 'ABS',
          'children' => array(),
        ),
        'Регуляторы тормозных усилий' => array(
          'translation' => 'KOREKTOR* HAMOW*',
          'children' => array(),
        ),
        'Тормоза барабанные' => array(
          'translation' => 250422,
          'children' => array(
            'Тормозные барабаны' => 'BĘB* HAMULC*',
            'Тормозные колодки' => 'SZCZ* hamulc*',
            'Цилиндрики тормозные' => 'CYLIND* HAMULC*',
          ),
        ),
        'Тормоза дисковые' => array(
          'translation' => 250402,
          'children' => array(
            'Скобы супортов' => 'Jarzm* zacisk*',
            'Супорта' => 'zacisk* hamulc*',
            'Томозные диски' => 'TARC* HAMULC*',
            'Тормозные колодки' => 'KLOCK* hamulc*',
          ),
        ),
        'Тормозные бачки' => array(
          'translation' => 'ZBIORN*  HAMUL*',
          'children' => array(),
        ),
        'Тормозные насосы' => array(
          'translation' => 'pomp* hamul*',
          'children' => array(),
        ),
        'Тормозные тросы' => array(
          'translation' => 'LIN* hamul*',
          'children' => array(),
        ),
        'Тормозные шланги' => array(
          'translation' => 'Przewód hamul*',
          'children' => array(),
        ),
      ),
    ),
    'Рулевое управление' => array(
      'translation' => 250842,
      'children' => array(
        'Бачки усилителя рулевого управления' => array(
          'translation' => 'ZBIORN* WSPOMAG*',
          'children' => array(),
        ),
        'Наконечники рулевых тяг' => array(
          'translation' => 'KONCOWK*',
          'children' => array(),
        ),
        'ГУР Усилители рулевого управления' => array(
          'translation' => 'POMP*  WSPOMAG*',
          'children' => array(),
        ),
        'Пыльники рулевой рейки' => array(
          'translation' => 'drąż* kierown*',
          'children' => array(),
        ),
        'Рулевые рейки' => array(
          'translation' => 'maglow*',
          'children' => array(),
        ),
        'Рулевые тяги' => array(
          'translation' => 'drąż* kierown*',
          'children' => array(),
        ),
        'Трубки шланги усилителя рулевого управления' => array(
          'translation' => 'WAZ* POMP*  WSPOMAG*',
          'children' => array(),
        ),
      ),
    ),
    'Салон - интерьер' => array(
      'translation' => 622,
      'children' => array(
        'GPS навигация' => array(
          'translation' => 'gps*',
          'children' => array(),
        ),
        'Автомобильные Прикуриватели' => array(
          'translation' => 'Żarnik*',
          'children' => array(),
        ),
        'Аудиосистемы' => array(
          'translation' => 250547,
          'children' => array(
            'CD/DVD чейнджеры' => 'CD/DVD чейнджеры',
            'Колонки' => 'GŁOŚN*',
            'Радиомагнитолы И Элементы' => 'radio',
            'Усилители' => 'wzmacn*',
          ),
        ),
        'Бардачки' => array(
          'translation' => 'SCHOW*',
          'children' => array(),
        ),
        'Шахты подлокотники' => array(
          'translation' => 'TUNEL*',
          'children' => array(),
        ),
        'Вентиляционные отверстия' => array(
          'translation' => 'KRATK* POWIETRZ*',
          'children' => array(),
        ),
        'Внутренние дверные ручки' => array(
          'translation' => 'KLAMK* WEWNĘTR*',
          'children' => array(),
        ),
        'Внутренняя отделка крыши' => array(
          'translation' => 'PODSUFITK*',
          'children' => array(),
        ),
        'Детали крепления' => array(
          'translation' => 'Mocow*',
          'children' => array(),
        ),
        'Задние полки' => array(
          'translation' => 'polk* tyl*',
          'children' => array(),
        ),
        'Защитные кожухи' => array(
          'translation' => 'MIESZ*',
          'children' => array(),
        ),
        'Зеркала внутренние' => array(
          'translation' => 'lusterko* WEWNĘTR*',
          'children' => array(),
        ),
        'Кресла, диваны' => array(
          'translation' => 'FOTEL*',
          'children' => array(),
        ),
        'Стеклоподьемники' => array(
          'translation' => 'MECHANIZM* SZYB*',
          'children' => array(),
        ),
        'Накладки, заглушки, облицовка' => array(
          'translation' => 'OSŁON*',
          'children' => array(),
        ),
        'Карточки дверные' => array(
          'translation' => 'Tapic*',
          'children' => array(),
        ),
        'Освещение салона' => array(
          'translation' => 'OŚWIETL* WNĘTR*',
          'children' => array(),
        ),
        'Панели управления, переключатели' => array(
          'translation' => 'PANEL*',
          'children' => array(),
        ),
        'Педали' => array(
          'translation' => 'PEDAL*',
          'children' => array(),
        ),
        'Пепельници' => array(
          'translation' => 'POPIELNICZK',
          'children' => array(),
        ),
        'Подголовники' => array(
          'translation' => 'ZAGŁÓW*',
          'children' => array(),
        ),
        'Подлокотники' => array(
          'translation' => 'PODLOKIETN*',
          'children' => array(),
        ),
        'Подушки безопасности' => array(
          'translation' => 'airbag',
          'children' => array(),
        ),
        'Приборные панели, консоли' => array(
          'translation' => 'KONSOL*',
          'children' => array(),
        ),
        'Ремни безопасности' => array(
          'translation' => 'PAS* BEZPIEC*',
          'children' => array(),
        ),
        'Ролеты багажника' => array(
          'translation' => 'rolet* bagazn*',
          'children' => array(),
        ),
        'Рули' => array(
          'translation' => 'KIEROWN*',
          'children' => array(),
        ),
        'Ручки переключения передач' => array(
          'translation' => 'GAŁK* ZMIAN* BIEGÓW*',
          'children' => array(),
        ),
        'Ручки, держатели, подстаканники' => array(
          'translation' => 'UCHWYT*',
          'children' => array(),
        ),
        'Солнцезащитные козырьки' => array(
          'translation' => 'OSŁON* PRZECIWSŁON*',
          'children' => array(),
        ),
        'Счетчики, часы' => array(
          'translation' => 'LICZN*',
          'children' => array(),
        ),
      ),
    ),
    'Сисиемы безопастности Air Bag' => array(
      'translation' => 'Konsol* airbag',
      'children' => array(
        'Комплекты подушек безопастности' => array(
          'translation' => 'Konsol* airbag',
          'children' => array(),
        ),
        'Подушка водителя' => array(
          'translation' => 'airbag',
          'children' => array(),
        ),
        'Подушка пассажира' => array(
          'translation' => 'airbag',
          'children' => array(),
        ),
        'Шторка' => array(
          'translation' => 'KURTYN*',
          'children' => array(),
        ),
        'Подушка коленная' => array(
          'translation' => 'airbag*',
          'children' => array(),
        ),
        'Блок SRS' => array(
          'translation' => 'SRS',
          'children' => array(),
        ),
        'Торпеда' => array(
          'translation' => 'Deska',
          'children' => array(),
        ),
      ),
    ),
    'Оптика' => array(
      'translation' => 623,
      'children' => array(
        'Передние фары' => array(
          'translation' => 'lamp* prz*',
          'children' => array(),
        ),
        'Задние фары' => array(
          'translation' => 'LAMP* TYL*',
          'children' => array(),
        ),
        'Галогенные фары' => array(
          'translation' => 'HALOGEN*',
          'children' => array(),
        ),
        'Лампы подсветки номерного знака' => array(
          'translation' => 'oświetl* tabl* rejestrac*',
          'children' => array(),
        ),
        'Приводы управления' => array(
          'translation' => 'SILN* REGUL* LAMP*',
          'children' => array(),
        ),
        'Стоп-сигналы' => array(
          'translation' => 'lamp* stop*',
          'children' => array(),
        ),
        'Поворотники' => array(
          'translation' => 'kierunkow*',
          'children' => array(),
        ),
      ),
    ),
    'Электроника - электрика' => array(
      'translation' => 4141,
      'children' => array(
        'Компьютеры ' => array(
          'translation' => 'KOMPut*',
          'children' => array(),
        ),
        'Блоки управления двигателем' => array(
          'translation' => 'KOMPut* Siln*',
          'children' => array(),
        ),
        'Блоки управления АКПП МКПП' => array(
          'translation' => 'Sterow* SKRZYN*',
          'children' => array(),
        ),
        'Блоки управления Другое' => array(
          'translation' => 'Sterow*',
          'children' => array(),
        ),
        'Датчики' => array(
          'translation' => 50744,
          'children' => array(
            'Давления' => 'Czujnik* Ciśnieni*',
            'Детонации сгорания' => 'Czujn* spalan* stukow*',
            'Дождя' => 'czujn* deszcz*',
            'Положения вала' => 'czujn* wal*',
            'Расходомеры' => 'Przepływom*',
            'Скорости' => 'CZUJN* PREDKOS*',
            'Температуры' => 'CZUJN*  temp*',
            'Уровень жидкости' => 'CZUJN*  ole*',
            'ЕКУ (ESP)' => 'CZUJN* ESP*',
          ),
        ),
        'Зажигание и ключи' => array(
          'translation' => 'KLUCZ*',
          'children' => array(),
        ),
        'Реле' => array(
          'translation' => 'PRZEKAŹN*',
          'children' => array(),
        ),
        'Электропроводка' => array(
          'translation' => 50751,
          'children' => array(
            'Предохранители' => 'SKRZYNK* BEZPIECZNIK*',
            'Проводка' => 'WIĄZK*',
            'Штекеры, разъемы' => 'KLEM* wtyczk*',
          ),
        ),
        'Центральные замки' => array(
          'translation' => 4144,
          'children' => array(
            'Привод замка' => 'zamek*',
            'Пульты' => 'zamek*',
          ),
        ),
        'Электрическая система двигателя' => array(
          'translation' => 50761,
          'children' => array(
            'Генераторы' => 'Alternat*',
            'Катушки зажигания' => 'cewk* zapł*',
            'Модули зажигания' => 'MODUŁ* zapłon*',
            'Провода зажигания' => 'WIĄZK* zapł*',
            'Стартеры' => 'rozruszn',
          ),
        ),
        'Электротехническое оборудование' => array(
          'translation' => 4147,
          'children' => array(
            'Автомобильные прикуриватели и гнезда' => 'Żarnik*',
            'Двигатели люков' => 'siln* szyberdach*',
            'Двигатели регулировки сиденья' => 'siln* regul*',
            'Двигатели стеклоочистителя' => 'siln* wycieraczk*',
            'Двигатели стеклоподъемника' => 'podnośnik* szyb*',
            'Клаксоны' => 'KLAKSON*',
            'Насосы омывателя' => 'POMP* SPRYSKIW*',
          ),
        ),
      ),
    ),
    'Колеса, диски' => array(
      'translation' => 18730,
      'children' => array(
        'Болты, гайки' => array(
          'translation' => 'NAKRETK* ŚRUB*',
          'children' => array(),
        ),
        'Датчики давления в шинах' => array(
          'translation' => 'Czujn* ciśnien* oponach*',
          'children' => array(),
        ),
        'Диски' => array(
          'translation' => 18731,
          'children' => array(
            'Алюминиевые' => 'Felg*',
            'Стальные' => 'Felg*  stal*',
          ),
        ),
        'Диски с шинами' => array(
          'translation' => 18731,
          'children' => array(
            'Алюминиевые' => 'Felg*  opon*',
            'Стальные' => 'Felg*  stal* opon*',
          ),
        ),
        'Доездные колеса, докатки' => array(
          'translation' => 'KOŁO* DOJAZDOW*',
          'children' => array(),
        ),
        'Колпаки' => array(
          'translation' => 49179,
          'children' => array(
            'Оригинальные' => 'KOŁPAK*  ORG*',
            'Универсальные' => 'KOŁPAK*',
          ),
        ),
        'Распорные втулки' => array(
          'translation' => 'Dystans*',
          'children' => array(),
        ),
        'Центрирующие кольца' => array(
          'translation' => 'pierścień* centrując*',
          'children' => array(),
        ),
      ),
    ),
    'Выхлопная система' => array(
      'translation' => 18862,
      'children' => array(
        'Глушители' => 'Tlum*',
        'Катализаторы' => 'kataliz*',
        'Лямбда-зонд' => 'sond* lambd*',
      ),
    ),
  );
  
  $array = array();
  
  if($category){
    if($type){
      if($subtype){
        if($translate) return @$spares[$category]['children'][$type]['children'][$subtype];
        return '';
      } else {
        if($translate) return @$spares[$category]['children'][$type]['translation'];
        $array = @$spares[$category]['children'][$type]['children'];
      }
    } else {
      if($translate) return @$spares[$category]['translation'];
      $array = @$spares[$category]['children'];
    }
  } else {
    if($translate) return '';
    $array = $spares;
  }
  
  if(!$array) return array();
  $array = array_keys($array);
  return array_combine($array, $array);
}