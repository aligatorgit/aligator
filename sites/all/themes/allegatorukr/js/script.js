(function($){
  
  window.alert = function(message) {
    if(typeof console != 'undefined') console.log(message);
  };
  
  Drupal.behaviors.customTheme = {
    attach: function (context, settings) {
      //Добавление увеличения и уменьшения в количетсво    
      $('input[name="quantity"], .basic-cart-cart-quantity input').once(function(){
        var jWrap = $(this).wrap('<span class="quantity">').parent();
      
        var jPlus = $('<span class="plus">+</span>').click(function(){
          var input = $(this).siblings('input');
          if(input.is(":disabled")) return;
          var val = parseInt(input.val())?parseInt(input.val()):0;
          input.val(val+1).change();
        });
        
        var jMinus = $('<span class="minus">-</span>').click(function(){
          var input = $(this).siblings('input');
          if(input.is(":disabled")) return;
          var val = parseInt(input.val())?parseInt(input.val())-1:0;
          if(val<0) val = 0;
          input.val(val).change();
        });
        
        jWrap.append(jPlus).prepend(jMinus);
      });   
      
      $('.basic-cart-delete span').once(function(){   
        $(this).click(function(){
          $(this)
            .closest('.row')
            .find('input')
            .val(0)
            .closest('.basic-cart-cart-form')
            .find('.basic-cart-call-to-action input:first')
            .click();
        });
      });
      
      $('.basic-cart-refresh span').once(function(){   
        $(this).click(function(){
          $(this)
            .closest('.basic-cart-cart-form')
            .find('.basic-cart-call-to-action input:first')
            .click();
        });
      });
      
      $('.product-wrapper .product-info .info .buy-form .add-to-cart').once(function(){
        $(this).click(function(){
          var itmID = $(this).attr('data-itm');
          var jInput = $(this).closest('.buy-form').find('input');
          var count = parseInt(jInput.val());
          var max = parseInt(jInput.attr('data-max'));
          
          if(count > max) {
            jInput.addClass('error');
            return false;
          } else {
            jInput.closest('.qty').remove();
          }
          
          var ajax = new Drupal.ajax(false, this, {url : '/cart/add/' + itmID + '/' + count + '/ajax'});
          ajax.eventResponse(ajax, {});
          return false;
        });
      });
    }
  }
  
  $(function(){
    
    /* Recommended Products Carousel */
    $('form.search-form').submit(function(){
      var query = $(this).find('input:hidden').not('[name="q"]').not('[name="in-description"]').not('[name="search"]').serialize();

      if(query.indexOf('description=') == -1){
        var description = parseInt($(this).find('[name="in-description"]').val());
        if(description) query += '&description=1';
      }
      
      if(query) query = '?' + query;
      
      var val = $(this).find("[name='search']").val();
      val = val.replace('\\', '');
      val = val.replace('/', '');
      val = encodeURIComponent(val);
      
      window.location = '/search/' + val + query;
      
      return false;
    })

    /* Recommended Products Carousel on Front Page */
    $('.front .view-recommended-products .owl-carousel').owlCarousel({
      items: 4,
      loop: true,
      nav: true,
      navText: ['Предыдущая', 'Следующая'],
      responsive : {
        0 : {
          items: 1
        },
        1001 : {
          items: 3
        },
        1201 : {
          items: 4
        },
      },
      onResize: function(){
        $(this).trigger('refresh.owl.carousel');
      }
    });

    /* Recommended Products Carousel on Product Page */
    $('.page-itm .view-recommended-products .owl-carousel').owlCarousel({
      items: 4,
      loop: true,
      nav: true,
      navText: ['Предыдущая', 'Следующая'],
      responsive : {
        0 : {
          items: 1
        },
        1001 : {
          items: 4
        },
        1201 : {
          items: 5
        },
      },
      onResize: function(){
        $(this).trigger('refresh.owl.carousel');
      }
    });

    /* Photo Carousel on Product Page */
    $('.page-itm .product-images .owl-carousel').owlCarousel({
      items: 3,
      loop: true,
      nav: true,
      navText: ['Предыдущая', 'Следующая'],
      onResize: function(){
        $(this).trigger('refresh.owl.carousel');
      }
    });
    
    /* Recommended Products Carousel on Avtorazborka Page */
    $('.node-type-article .view-recommended-products .owl-carousel').each(function(){
      var jOrigin = $(this);
      var jClone = jOrigin.clone().insertAfter(this);
      var refresh = function(){        
        if($(window).width() <= 1000) {
          jOrigin.hide();
          jClone.show();
          jClone.trigger('refresh.owl.carousel');
        } else {
          jOrigin.show();
          jClone.hide();            
        }
      };
      
      jOrigin.addClass('owl-not-loaded');
      jClone.owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        onInitialize: refresh,
        onResize: refresh
      });
    });
    
    /* Last Articles Carousel */
    $('.view-last-articles .owl-carousel').owlCarousel({
      items: 3,
      loop: false,
      nav: true,
      navText: ['Предыдущая', 'Следующая'],
      responsive : {
        0 : {
          items: 1
        },
        1001 : {
          items: 3
        },
      },
      onResize: function(){
        $(this).trigger('refresh.owl.carousel');
      }
    });
    
    /* Zoom Photo */
    $('.product-wrapper .product-images .image img').elevateZoom({
      zoomType: 'inner'
    });
    
    /* Mobile Menu Icon */
    $('<span class="menu"></span>').click(function(){
      $('body > nav ul').toggleClass('opened');
    }).appendTo('body > nav .wrap');
    
    /* Fixed header */
    $('body > header').clone().each(function(){
      $(this).find('div.spares, div.phones').remove();
      $(this).addClass('fixed').attr('id', 'fixed').appendTo('body');
    });
    
    /* Scroller */
    $('<div id="up"></div>').appendTo('body').click(function(){
      $('html,body').animate({scrollTop: 0})          
      return false;
    });
    $(window).scroll(function(){
      if($(this).scrollTop() > 200) {
        $('#fixed').addClass('shown');
        $('#up').fadeIn();
      } else {
        $('#fixed').removeClass('shown');
        $('#up').fadeOut();
      }
    });
    
    /* Redirect After Checkout */
    if($('body.page-checkout-thank-you').length){
      setTimeout(function(){
        window.location = '/';
      }, 15000);
    }
        
  });
  
})(jQuery);
//$&$-begin
/*function googleTranslateElementInit() {
        var pl_phrases_arr = [];
        var subtitles_arr = document.getElementsByClassName('subtitle');

        for (var i = 0; i < subtitles_arr.length; i ++){
          pl_phrases_arr.push(subtitles_arr[i].textContent);
        }

        googleSetCookie('G_AUTHUSER_H', '0');
        googleSetCookie('googtrans', '/pl/ru');

        new google.translate.TranslateElement(
            {pageLanguage: 'pl'},
            'google_translate_element'
        );

        jQuery('.goog-te-banner').css('display', 'none');
        jQuery('.goog-te-combo').css('display', 'none');
        jQuery('.goog-logo-link').css('display', 'none');
        jQuery('.goog-te-gadget').css('font-size', '0');

        //jQuery('.skiptranslate').remove();
        //$('body').css('top', '');

        var translated = false;
        document.body.addEventListener("DOMNodeInserted",function(e){
          if(e.relatedNode.className == "goog-te-combo" && subtitles_arr[subtitles_arr.length - 1].children[0] && subtitles_arr[subtitles_arr.length - 1].children[0].children && (!translated)){
            translated = true;
            for (var i = 0; i < subtitles_arr.length; i ++){
              subtitles_arr[i].children[0].children[0].textContent = '' + pl_phrases_arr[i];
            }
          }

          jQuery('.skiptranslate').css('display', 'none');
          jQuery('body').css('top', '');
        }, false);
    }

    function googleSetCookie(name, value, options) {
        options = options || {};

          var expires = options.expires;

          if (typeof expires == "number" && expires) {
           var d = new Date();
           d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
         }
          if (expires && expires.toUTCString) {
           options.expires = expires.toUTCString();
        }

         value = encodeURIComponent(value);

        var updatedCookie = name + "=" + value;

        for (var propName in options) {
            updatedCookie += "; " + propName;
            var propValue = options[propName];
            if (propValue !== true) {
              updatedCookie += "=" + propValue;
            }
        }

        document.cookie = updatedCookie;
    }*/
    //$&$-end