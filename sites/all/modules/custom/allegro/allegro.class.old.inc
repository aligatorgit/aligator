<?php

class AllegroClass {
  private $login;
  private $password;
  private $key;
  private $session;
  private $client;
  public function __construct(){
    $this->login     = variable_get('allegro_login');
    $this->password  = variable_get('allegro_password');
    $this->key       = variable_get('allegro_key');
    $this->country   = 1;
    $this->session   = variable_get('allegro_session');
    $this->client    = new SoapClient('https://webapi.allegro.pl/service.php?wsdl');
    //variable_set('allegro_session', '');
    //$this->login();
  }
  private function login(){
    try{
      $version = $this->client->doQuerySysStatus(array(
        'sysvar' => 1, 
        'countryId' => $this->country, 
        'webapiKey' => $this->key,
      ));
      $info = $this->client->doLogin(array(
        'userLogin' => $this->login, 
        'userPassword' => $this->password, 
        'countryCode' => $this->country, 
        'webapiKey' => $this->key, 
        'localVersion' => $version->verKey,
      ));
      $this->session = $info->sessionHandlePart;
      variable_set('allegro_session', $this->session);
    } catch(SoapFault $error) {
      return false;
    }
    return true;
  }
  public function search($key=null, $category=620, $page=0, $limit=10, $condition='all', $type='all', $from=0, $to=0, $sort=null, $description=0, $recommended=null, $seller = ''){
    if(!in_array($condition, array('new', 'used'))) $condition = null;
    if(!in_array($type, array('buyNow', 'auction'))) $type = null;
    if($from > $to) $to = 9999999999999;
    if(!in_array($sort, array('endingTime', 'price', 'popularity'))) $sort = null;
    else $order = $sort == 'endingTime' ? 'asc' : 'desc';
    
    $query = array(
      'webapiKey' => $this->key, 
      'countryId' => $this->country,
      'filterOptions' => array(),
      'sortOptions' => array(),
      'resultOffset' => $page * $limit,
      'resultSize' => $limit,
    );
    
    if($key) $query['filterOptions'][] = array(
      'filterId' => 'search',
      'filterValueId' => array($key),
    );

    if($category) $query['filterOptions'][] = array(
      'filterId' => 'category',
      'filterValueId' => array($category),
    );

    if($condition) $query['filterOptions'][] = array(
      'filterId' => 'condition',
      'filterValueId' => array($condition),
    );

    if($type) $query['filterOptions'][] = array(
      'filterId' => 'offerType',
      'filterValueId' => array($type),
    );

    if($from || $to) $query['filterOptions'][] = array(
      'filterId' => 'price',
      'filterValueRange' => array(
        'rangeValueMin' => $from,
        'rangeValueMax' => $to,
      ),
    );
    
    if($sort) $query['sortOptions'] = array(
      'sortType' => $sort,
      'sortOrder' => $order,
    );
    
    if($description) $query['filterOptions'][] = array(
      'filterId' => 'description',
      'filterValueId' => array(true),
    );
    
    if($recommended) $query['filterOptions'][] = array(
      'filterId' => 'offerOptions',
      'filterValueId' => array('standardAllegro'),
    );
    
    if($seller) $query['filterOptions'][] = array(
      'filterId' => 'userId',
      'filterValueId' => array($seller),
    );

    try { 
      $result = $this->client->doGetItemsList($query);
    } catch(SoapFault $error) {
      dsm($error);
      return;
    }
    
    if (!empty($result->categoriesList->categoriesPath->item)) {
      if (!is_array($result->categoriesList->categoriesPath->item)) return -1;
      if ($result->categoriesList->categoriesPath->item[0]->categoryId != 3) return -1;
      if ($result->categoriesList->categoriesPath->item[1]->categoryId != 620) return -1;
    }

    //echo 'allegro class result<br>';
    //var_dump($result);
    //echo '<br>';
    return $result;
  }
  public function product($id){
    $options = array(
      'sessionHandle' => $this->session, 
      'itemId' => $id, 
      'getDesc' => 1, 
      'getImageUrl' => 1, 
      'getAttribs' => 1, 
      'getPostageOptions' => 1,
    );
    
    try { 
      $result = $this->client->doShowItemInfoExt($options);
    } catch(SoapFault $error) {
      dsm($error);
      if(in_array($error->faultcode, array('ERR_NO_SESSION', 'ERR_SESSION_EXPIRED'))){
        if(!$this->login()) return;
        $result = $this->client->doShowItemInfoExt($options);
      } else return;
    }
    
    return $result;
  }
  public function getCategories(){
    return $this->client->doGetCatsData(1, 0, $this->key);
  }
}