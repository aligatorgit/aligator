<?php

$dictionary = array();

/**
 * Translate text by yandex
 * Yandex API documentation: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/
 */

function tpr($text) {
  $is_array = is_array($text);
  if(!$is_array) $text = array($text);
  
  $response = @gethtml('https://translate.yandex.net/api/v1.5/tr.json/translate?' .  http_build_query(array(
    //'key' => variable_get('allegro_yandex'),
    //'key' => 'trnsl.1.1.20170822T064559Z.b699d465b112c564.10053b346e0330f0aac37b6829805ed1a17c8002',
    'key' => 'trnsl.1.1.20170825T142043Z.5b590c8761e39754.9224a5dcd7a4abf1fe519b964ba27f9b0a59b69f',
    'text' => implode("\n", $text),
    'lang' => 'pl-ru',
    'format' => 'plain',
  )));
  
  if($response){
    $response = json_decode($response);
    if($response && $response->code == 200){
      $final = array();
      $translations = explode("\n", $response->text[0]);
      foreach($translations as $k => $translation){
        if(!$translation) continue;
        $final[$text[$k]] = $translation;
      }
      return $is_array ? $final : implode("\n", $final);
    }
  }

  return $is_array ? array() : '';
}

function tpr1($text) {
  if($is_array = is_array($text)) $text = implode("\n", $text);
  
  $url = sprintf('http://translate.google.ru/translate_a/t?client=t&hl=en&sl=%s&tl=%s&ie=UTF-8&oe=UTF-8&multires=1&otf=1&pc=1&trs=1&ssel=3&tsel=6&sc=1', 'pl', 'ru');
  
  $translation = @gethtml($url, 'text=' . rawurlencode($text));

  $translation = preg_replace('!,+!', ',', $translation); // remove repeated commas (causing JSON syntax error)
  $translation = str_replace('[,', '[', $translation);
  $translation = json_decode($translation, true);  
  
  $final = '';
  if(!empty($translation[0])) {
    foreach($translation[0] as $results) {
      $final[trim($results[1])] = $results[0];
    }
    return $is_array ? $final : implode("\n", $final);
  }
  
  return $is_array ? array() : '';
}

function tpr2($text, $db = true){
  static $cache = array();
  
  if(isset($cache[$text])) return $cache[$text];
  
  //Расставить индексы
  if($db){
    //Лезем в БД
    $result = db_select('allegro_translate', 't')
      ->fields('t')
      ->condition('t.pl', $text)
      ->execute()
      ->fetchObject();
    if($result) {
      $cache[$text] = $result->ru;
      return $result->ru;
    }
  }
  
  //Переводим
  $translation = @gethtml('http://translate.google.ru/translate_a/t?client=x&text='.str_replace(' ', '+', $text).'&hl=pl&sl=pl&tl=ru&ie=UTF-8&oe=UTF-8');   
  $translation = @json_decode($translation);
  $translation = @$translation->sentences[0]->trans;
  if(!$translation) return $text;
  
  if($db){
    //Записываем в БД
    $translation = mb_ucfirst($translation);
    db_insert('allegro_translate')
      ->fields(array(
        'ru' => $translation,
        'pl' => mb_ucfirst($text),
      ))
      ->execute(); 
  }
  
  $cache[$text] = $translation;
    
  return $translation;
}

//$&$-begin///////////////////////////////////////////////////////////////////////////////
function parsePlStringsArray($pl_strings_array){
  global $dictionary;
  
  foreach ($pl_strings_array as $pl_string) {

    $pl_array = explode(' ', $pl_string);
    $phrases_collection_arr = array();
    $phrases_arrays_arr = array();

    $cnt = 0;
    for($i = 0; $i < count($pl_array); $i++){
      for($j = $i; $j < count($pl_array); $j++){
        $phrases_arrays_arr[$cnt] = array();
        for($k = $i; $k <= $j; $k++){
          $phrases_arrays_arr[$cnt][] = $pl_array[$k];
        }
        $cnt++;
      }
    }

    $query_strings_arr = array();
    foreach ($phrases_arrays_arr as $str_arr) {
      $query_strings_arr[] = implode(' ', $str_arr);
    }

    $result = db_select('allegro_translate_pr_addition', 't')
      ->fields('t', array('pl', 'ru'))
      ->condition('t.pl', $query_strings_arr, 'IN')
      ->execute()
      ->fetchAllKeyed(0, 1);

    uksort($result, function($a, $b){
      $a_l = strlen($a);
      $b_l = strlen($b);

      if ($a == $b) {
        return 0;
      }
      return ($a_l > $b_l) ? -1 : 1;
    });

    $str_to_replace = $pl_string . ' ';

    if(!empty($result)){
      foreach ($result as $k => $v) {
        $k .= ' ';
        $v .= ' ';

        //echo 'k - '. $k . '<br>';
        //echo 'v - '. $v . '<br>';
        //echo 'str_to_replace - '. $str_to_replace . '<br>';


        $str_to_replace = str_ireplace($k, $v, $str_to_replace);
        //echo 'str_to_replace - '. $str_to_replace . '<br><br><br>';
      }
      if(empty($dictionary[$pl_string]['t'])){
        $dictionary[$pl_string]['t'] = $str_to_replace;
        //echo 'str_to_replace - '. $str_to_replace . '<br>';
        //echo 'pl_string - '. $pl_string . '<br><br>';
      }
    }
  }

  return $pl_strings_array;
}
//$&$-end///////////////////////////////////////////////////////////////////////////////

function translate_correct_tpr(){
  //echo 'translate_correct_tpr'; - only after cleaning cache
  global $dictionary;

  /*echo 'translate_correct_tpr - dictionary:';
  var_dump($dictionary);
  echo '<br><br><br>';*/
  
  if(empty($dictionary) || !is_array($dictionary)) $dictionary = array(); 
  
  //Собираем непереведенные слова
  $words = array();
  foreach($dictionary as $row){
    if(!empty($row['t'])) continue;
    foreach($row['c'] as $context){
      if($context['lng'] == 'pl') $words[] = $context['text'];
    }
  }


  //$&$-begin///////////////////////////////////////////////////////////////////////////////

  //$words = parsePlStringsArray($words);
  parsePlStringsArray($words);

  foreach ($words as $key => $value) {
    $pl_words_array = array();
    $pl_words_array = explode(" ", $value);

    foreach ($pl_words_array as $k => $v) {
      if(is_numeric($v)) unset($pl_words_array[$k]);
    }

    if(!empty($pl_words_array)){
      $result = db_select('allegro_translate_pr_addition', 't')
        ->fields('t', array('pl', 'ru'))
        ->condition('t.pl', $pl_words_array, 'IN')
        ->execute()
        ->fetchAllKeyed(0, 1);
    }
    
    /*$result_ = array();

    foreach ($result as $k => $v) {
      $result_[$k . ' '] = $v . ' ';
    }

    $str_to_replace = $value . ' ';

    if(!empty($result_)){
      $translated_str = str_ireplace(array_keys($result_), array_values($result_), $str_to_replace);
      if(empty($dictionary[$value]['t'])) $dictionary[$value]['t'] = $translated_str;
    }*/
    
    $check_res_arr = array_change_key_case($result, CASE_LOWER);

    foreach ($pl_words_array as $k => $v) {
      if(!array_key_exists(strtolower($v), $check_res_arr)){
        $translate = tpr(array(0 => $v));

        foreach ($translate as $pl => $ru){
          db_insert('allegro_translate_pr_addition')
            ->fields(array(
              'ru' => $ru,
              'pl' => $pl,
            ))
            ->execute();
        }
      }
    }
  }
  //$&$-end//////////////////////////////////////////////////////////////////////////////

  //Переводим слова
  //$&$-commented $translate = tpr($words);
  
  //Заменяем переведенные слова
  /*$&$ - commented
  foreach($dictionary as $text => $row){
    if(!empty($row['t'])) continue;
    
    $words = array();
    foreach($row['c'] as $context){
      if($context['lng'] == 'pl') {
        if(isset($translate[$context['text']])) $words[] = $translate[$context['text']];
        else $words[] = $context['text'];
      } else {
        $words[] = $context['text'];
      }
    }
    
    $dictionary[$text]['t'] = implode(' ', $words);
  }*/
}

function prepare_correct_tpr($text, $parse = FALSE){
  global $dictionary;
  
  if(!isset($dictionary[$text])) {
    $dictionary[$text] = array(
      'o' => $text,
      't' => '',
      'c' => array(),
    );
    correct_tpr($text, $parse);
  }
}

function correct_tpr($text, $parse = FALSE){
  global $dictionary;

  if(!$text) return $text;
  
  //Если есть перевод, то возвращаем его
  if(!empty($dictionary[$text]['t'])) return $dictionary[$text]['t'];
  
  //Если есть контекст возвращаем его
  if(!empty($dictionary[$text]['c'])) {
    $context = $dictionary[$text]['c'];
  } elseif($parse) {
    //Разбиваем текст на слова и создаем селекторы для запроса
    $origin = explode(' ', $text);
    $selectors = array();
    for($i=0; $i<count($origin); $i++){
      if(!($new_text = trim($origin[$i]))) continue;
      $selectors[] = $new_text;
      for($j=$i+1; $j<count($origin); $j++){
        if(!trim($origin[$j])) continue;
        $selectors[] = $new_text = $new_text . ' ' . trim($origin[$j]);
      }
    }
    
    //Выбираем селекторы из БД (не учитывается регистр)
    $words = db_select('allegro_translate_pr_addition', 't')
      ->fields('t', array('pl', 'ru'))
      ->condition('t.pl', $selectors, 'IN')
      ->execute()
      ->fetchAllKeyed(0, 1); 
      
    //Сортируем селекторы по количеству слов
    uasort($words, function($a, $b){
      $a = count(explode(' ', $a));
      $b = count(explode(' ', $b));
      return $b - $a;
    });
    
    //Заменяем переводы
    $translate = $text;

    //$&$-begin////////////////////////////
    $translate .= ' ';    
    foreach ($words as $pl => $ru) {
      $translate = str_replace($pl . ' ', $ru . ' ', $translate);
    }
    //$&$-end//////////////////////////////

    foreach($words as $pl=>$ru){
      //$&$-commented $translate = str_ireplace($pl, $ru, $translate);

      //$&$-begin////////////////////////////
      $translate = str_ireplace($pl . ' ', $ru . ' ', $translate);
      //$&$-end//////////////////////////////
    }

    //Разбиваем переведенное слово
    $origin = explode(' ', $translate);
    $context = array(); $count = 0;
    for($i=0; $i<count($origin); $i++){
      $lng = preg_match('/^[\x80-\xFF]+$/', $origin[$i])?'ru':'pl';
      
      if(!isset($context[$count])){
        $context[$count] = array(
          'lng' => $lng,
          'text' => $origin[$i],
        );
      } else {
        if($context[$count]['lng']==$lng) {
          $context[$count]['text'] .= ' '.$origin[$i];
        } else {
          $count++;
          $context[$count] = array(
            'lng' => $lng,
            'text' => $origin[$i],
          );
        }
      }
    }
    
    //Сохраняем контекст переводов
    $dictionary[$text]['c'] = $context;
  } else {
    //Переводим слово полностью
    $context[] = array(
      'lng' => 'pl',
      'text' => $text,
    );
    
    //Сохраняем контекст переводов
    $dictionary[$text]['c'] = $context;
  }
  
  //Соединяем переводы
  $translate = array(); $partical = FALSE;
  foreach($context as $tr){
    if($tr['lng']=='pl') {
      $translate[] = $tr['text'];// = tpr($tr['text'], false);
      $partical = TRUE;
    } else {
      $translate[] = $tr['text'];
    }
  }
  $translate = implode(' ', $translate);
  
  //Если перевод не частичный, то сохраняем его
  if(!$partical) $dictionary[$text]['t'] = $translate;
  
  return $translate;
}

function trp($text){  
  $result = db_select('allegro_translate', 't')
    ->fields('t')
    ->condition('t.ru', $text)
    ->execute()
    ->fetchObject();
  
  if($result) return $result->pl;
  
  $response = @gethtml('https://translate.yandex.net/api/v1.5/tr.json/translate?' .  http_build_query(array(
    //'key' => variable_get('allegro_yandex'),
    'key' => 'trnsl.1.1.20170822T064559Z.b699d465b112c564.10053b346e0330f0aac37b6829805ed1a17c8002',
    'text' => $text,
    'lang' => 'ru-pl',
    'format' => 'plain',
  )));
  
  if(!$response) return $text;
  $response = json_decode($response);
  if(!$response || $response->code != 200) return $text;
  
  $translate = @$response->text[0];
  
  if(!$translate) return $text;
  
  $translate = mb_ucfirst($translate);
  $text = mb_ucfirst($text);
  
  db_insert('allegro_translate')
    ->fields(array(
      'ru' => $text,
      'pl' => $translate,
    ))
    ->execute(); 
    
  return $translate;
}

function trp1($text){  
  $result = db_select('allegro_translate', 't')
    ->fields('t')
    ->condition('t.ru', $text)
    ->execute()
    ->fetchObject();
  
  if($result) return $result->pl;
  
  $translate = @gethtml('http://translate.google.ru/translate_a/t?client=x&text='.str_replace(' ', '+', $text).'&hl=ru&sl=ru&tl=pl&ie=UTF-8&oe=UTF-8');
  $translate = @json_decode($translate);
  $translate = @$translate->sentences[0]->trans;
  
  if(!$translate) return $text;
  
  $translate = mb_ucfirst($translate);
  $text = mb_ucfirst($text);
  
  db_insert('allegro_translate')
    ->fields(array(
      'ru' => $text,
      'pl' => $translate,
    ))
    ->execute(); 
    
  return $translate;
}

function correct_trp($text){
  $translate = $text;
  $text = explode(' ', $text);
  
  $selectors = array();
  for($i=0; $i<count($text); $i++){
    if(!($new_text = trim($text[$i]))) continue;
    $selectors[] = $new_text;
    for($j=$i+1; $j<count($text); $j++){
      if(!trim($text[$j])) continue;
      $selectors[] = $new_text = $new_text . ' ' . trim($text[$j]);
    }
  }
  
  usort($selectors, function($a, $b){
    $a = count(explode(' ', $a));
    $b = count(explode(' ', $b));
    if($a == $b) return 0;
    return ($a > $b) ? -1 : 1;
  });
  
  $words = array();
  foreach($selectors as $selector){
    $result = db_select('allegro_translate_rp_addition', 't')
      ->fields('t')
      ->condition('t.ru', $selector, 'LIKE')
      ->execute()
      ->fetchObject(); 
    if(!$result) continue;
    $words[$selector] = $result->pl;
  }
  
  foreach($words as $ru=>$pl){
    $translate = str_replace($ru, $pl, $translate);
  }
  
  $text = explode(' ', $translate);
  $lng_translate = array(); $count = 0;
  for($i=0; $i<count($text); $i++){
    $lng = preg_match('/^[\x80-\xFF]+$/', $text[$i])?'ru':'pl';
    
    if(!isset($lng_translate[$count])){
      $lng_translate[$count] = array(
        'lng' => $lng,
        'text' => $text[$i],
      );
    } else {
      if($lng_translate[$count]['lng']==$lng) {
        $lng_translate[$count]['text'] .= ' '.$text[$i];
      } else {
        $count++;
        $lng_translate[$count] = array(
          'lng' => $lng,
          'text' => $text[$i],
        );
      }
    }
  }
  
  $translate = array();
  foreach($lng_translate as $tr){
    if($tr['lng']=='ru') $translate[] = trp($tr['text']);
    else $translate[] = $tr['text'];
  }
  $translate = implode(' ', $translate);
  
  return $translate;
}

if(!function_exists('mb_ucfirst')) {
  function mb_ucfirst($str, $enc = 'utf-8') { 
    return mb_strtoupper(mb_substr($str, 0, 1, $enc), $enc).mb_substr($str, 1, mb_strlen($str, $enc), $enc); 
  }
}

function gethtml($url, $post = FALSE){

  $curl = curl_init(); 
	 
	$header[] = "Accept:*/*"; 
	$header[] = "Accept-Encoding:gzip,deflate,sdch"; 
	$header[] = "Accept-Language:ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4"; 
	$header[] = "Connection:keep-alive"; 
	$header[] = "Host:translate.google.com"; 
	
	////////////////////////////////////////////////////////
	// $proxy = '193.194.69.36:3128'; // 136.243.4.136:10000 (193.194.69.36:3128 - quicker and no need to CURLOPT_SSL_VERIFYPEER and 	  	
	//								  // CURLOPT_SSL_VERIFYHOST)
	// curl_setopt($curl, CURLOPT_PROXY, $proxy);
	// curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
	// curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
	/////////////////////////////////////////////////////// 

	curl_setopt($curl, CURLOPT_URL, $url); 
	curl_setopt($curl, CURLOPT_USERAGENT, $_SERVER["HTTP_USER_AGENT"]); 
	curl_setopt($curl, CURLOPT_HTTPHEADER, $header); 
	curl_setopt($curl, CURLOPT_REFERER, 'http://translate.google.com/'); 
	curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate'); 
	curl_setopt($curl, CURLOPT_AUTOREFERER, true); 
	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1); 
  	curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true); 
	curl_setopt($curl, CURLOPT_TIMEOUT, 4);  
  	
	if($post){
    curl_setopt($curl, CURLOPT_POST, true);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $post);
	}
	
	$result = curl_exec($curl);
	curl_close($curl);
	
	return $result; 
}